
CREATE TABLE `powerball_data` (
	`no` INT(11) NOT NULL AUTO_INCREMENT COMMENT '순번',
	`date` DATE NOT NULL COMMENT '일자',
	`round` INT(11) NOT NULL COMMENT '회차',
	`total_round` INT(11) NOT NULL COMMENT '전체회차',
	`status` VARCHAR(1) NULL DEFAULT NULL COMMENT '상태 (R:대기, D:완료, C:취소)',
	`balls` VARCHAR(20) NULL DEFAULT NULL COMMENT '당첨번호',
	`powerball` VARCHAR(20) NULL DEFAULT NULL COMMENT '파워볼',
	`sum_numbers` VARCHAR(20) NULL DEFAULT NULL COMMENT '숫자합',
	`size` VARCHAR(1) NULL DEFAULT NULL COMMENT '대중소(L:대,M:중,S:소)',
	`oe` VARCHAR(1) NULL DEFAULT NULL COMMENT '일반볼합 홀짝(O:홀, E:짝)',
	`power_oe` VARCHAR(1) NULL DEFAULT NULL COMMENT '파워볼홀짝(O:홀,E:짝)',
	`ou` VARCHAR(1) NULL DEFAULT NULL COMMENT '일반볼합 언오버(U:언더,O:오버)',
	`power_ou` VARCHAR(1) NULL DEFAULT NULL COMMENT '파워볼 언오버(U:언더,O:오버)',
	`section` VARCHAR(1) NULL DEFAULT NULL COMMENT '일반볼 구간',
	`power_section` VARCHAR(1) NULL DEFAULT NULL COMMENT '파워볼 구간',
	`create_datetime` DATETIME NULL DEFAULT NULL COMMENT '등록일자',
	`update_datetime` DATETIME NULL DEFAULT NULL COMMENT '변경일자',
	PRIMARY KEY (`no`),
	UNIQUE INDEX `date_round` (`date`, `round`),
	UNIQUE INDEX `total_round` (`total_round`),
	INDEX `date` (`date`)
)
COMMENT='파워볼 기본 데이타'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;



-- 테이블 powerball_parsing_log 구조 내보내기
CREATE TABLE IF NOT EXISTS `parsing_log` (
  `no` int(11) NOT NULL AUTO_INCREMENT COMMENT '순번',
  `date` date NOT NULL COMMENT '일자',
  `game_type` varchar(50) NOT NULL COMMENT '게임타입',
  `parsing_url` varchar(255) DEFAULT NULL COMMENT '파싱대상 url',
  `response_status` varchar(10) DEFAULT NULL COMMENT '파싱결과 상태(200:정상, 그외 : 오류)',
  `response_body` varchar(1000) DEFAULT NULL COMMENT '파싱 결과 메세지',
  `response_time` int(11) NOT NULL DEFAULT '0' COMMENT '경과시간(밀리세컨드)',
  `create_datetime` datetime NOT NULL COMMENT '일시',
  PRIMARY KEY (`no`),
  KEY `date` (`date`),
  KEY `game_type` (`game_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='파싱 로그';

