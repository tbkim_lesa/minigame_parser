package com.pickchat.minigame;

import com.pickchat.minigame.dhlottery.PowerballParser;
import com.pickchat.minigame.dhlottery.PowerballSecondParser;
import com.pickchat.minigame.dhlottery.PowerballThirdParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MinigameCrawler {

    private static Logger logger = LoggerFactory.getLogger(MinigameCrawler.class);


    public static void main(String[] args) {
        try {
            if (args.length >= 1 && args[0].equals("all")) { //파서를 모두 실행하는 경우(ntry, livescore)
                ScheduledExecutorService scheduledExecutor = Executors.newSingleThreadScheduledExecutor();
                ScheduledExecutorService scheduledExecutor2 = Executors.newSingleThreadScheduledExecutor();
                scheduledExecutor.scheduleWithFixedDelay(new PowerballSecondParser(), 5 * 1000, 1000, TimeUnit.MILLISECONDS);
                scheduledExecutor2.scheduleWithFixedDelay(new PowerballThirdParser(), 10 * 1000, 1000, TimeUnit.MILLISECONDS);
            } else if(args.length >= 1 && args[0].equals("ntry")) { //ntry 1개만 실행
                ScheduledExecutorService scheduledExecutor = Executors.newSingleThreadScheduledExecutor();
                SimpleDateFormat korDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                korDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
                String today = korDateFormat.format(new Date());
                scheduledExecutor.scheduleWithFixedDelay(new PowerballSecondParser(), 5 * 1000, 1000, TimeUnit.MILLISECONDS);
            } else if(args.length >= 1 && args[0].equals("livescore")) {  //livescore 1개만 실행
                ScheduledExecutorService scheduledExecutor = Executors.newSingleThreadScheduledExecutor();
                SimpleDateFormat korDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                korDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
                String today = korDateFormat.format(new Date());
                scheduledExecutor.scheduleWithFixedDelay(new PowerballThirdParser(), 5 * 1000, 1000, TimeUnit.MILLISECONDS);
            } else { //그외는 동행복권만 실행
                ScheduledExecutorService scheduledExecutor = Executors.newSingleThreadScheduledExecutor();
                SimpleDateFormat korDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                korDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
                String today = korDateFormat.format(new Date());
                PowerballParser powerballParser = new PowerballParser(today);
                scheduledExecutor.scheduleWithFixedDelay(powerballParser, 0, 1000, TimeUnit.MILLISECONDS);
            }
        } catch(Exception ex) {
            Logger logger = LoggerFactory.getLogger(MinigameCrawler.class);
            logger.error(ex.getMessage(), ex);
        }

    }

}
