package com.pickchat.minigame.Exception;

public class ClientException extends Exception {

    int errorCode = 500;
    public ClientException(String strMsg, int errorCode) {
        super(strMsg);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
