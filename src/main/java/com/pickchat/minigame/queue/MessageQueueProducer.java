package com.pickchat.minigame.queue;



import com.pickchat.minigame.dhlottery.config.Configuration;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.*;

public class MessageQueueProducer {

    private static final Logger logger = LoggerFactory.getLogger(MessageQueueProducer.class);

    private static String rabbitmq_host;
    private static boolean rabbitmq_enable;
    private static int rabbitmq_port;
    private static String rabbitmq_username;
    private static String rabbitmq_password;
    private static String rabbitmq_exchange_name;

    static {

        String resource = "setting.properties";
        Properties prop = new Properties();

        try {
            InputStream in = MessageQueueProducer.class.getClassLoader().getResourceAsStream(resource);
            prop.load(in);
            in.close();

            String server_instance = Configuration.getServerInstance();

            String enable = prop.getProperty(server_instance + ".rabbitmq.enable");
            rabbitmq_enable = (enable != null && enable.equals("1")) ? true : false;
            rabbitmq_host = prop.getProperty(server_instance + ".rabbitmq.host") != null ? prop.getProperty(server_instance + ".rabbitmq.host") : "127.0.0.1";
            rabbitmq_port = prop.getProperty(server_instance + ".rabbitmq.port") != null ? Integer.parseInt(prop.getProperty(server_instance + ".rabbitmq.port")) : 5672;
            rabbitmq_username = prop.getProperty(server_instance + ".rabbitmq.username") != null ? prop.getProperty(server_instance + ".rabbitmq.username") : "";
            rabbitmq_password = prop.getProperty(server_instance + ".rabbitmq.password") != null ? prop.getProperty(server_instance + ".rabbitmq.password") : "";
            rabbitmq_exchange_name = prop.getProperty(server_instance + ".rabbitmq.exchange_name") != null ? prop.getProperty(server_instance + ".rabbitmq.exchange_name") : "pickchat_dev";

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-2);
        }
    }

    public MessageQueueProducer() {

    }


    /**
     *
     * @param message json encoded 된 string
     * @param topic 이벤트 메세지를 전송할 topic, 축구인 경우 pickchat/livescore/football
     *              파워볼인 경우  pickchat/minigame/powerball
     * @return
     * @throws Exception
     */
    public boolean sendTopicMessage(String message, String topic) {


        if(message == null) return false;

        if(!rabbitmq_enable) return false;

        try {

            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(rabbitmq_host);
            factory.setPort(rabbitmq_port);
            factory.setUsername(rabbitmq_username);
            factory.setPassword(rabbitmq_password);
            try (Connection connection = factory.newConnection();
                 Channel channel = connection.createChannel()) {

                String routingKey = StringUtils.replace(topic.toLowerCase(), "/", ".");
                channel.exchangeDeclare(rabbitmq_exchange_name, "topic");
                channel.basicPublish(rabbitmq_exchange_name, routingKey, null, message.getBytes("UTF-8"));
                logger.info(" [x] Sent '{}':'{}'", routingKey, message);
            }
        } catch(Exception ex) {
            logger.error(ex.getMessage());
        }

        return true;
    }

    public static void main(String[] argv) throws Exception {

    }


}

