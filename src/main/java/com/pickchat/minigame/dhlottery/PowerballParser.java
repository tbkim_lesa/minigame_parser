package com.pickchat.minigame.dhlottery;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pickchat.minigame.crawler.service.PushMessageService;
import com.pickchat.minigame.crawler.service.TelegramMessageService;
import com.pickchat.minigame.crawler.service.WebCrawlingService;

import com.pickchat.minigame.dhlottery.bean.Powerball;
import com.pickchat.minigame.dhlottery.bean.PowerballPush;
import com.pickchat.minigame.dhlottery.bean.TimelinePush;
import com.pickchat.minigame.dhlottery.config.Configuration;
import com.pickchat.minigame.dhlottery.dao.PowerballDAO;
import com.pickchat.minigame.queue.MessageQueueProducer;
import org.apache.commons.lang3.StringUtils;

import org.apache.http.impl.client.BasicCookieStore;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by taebong.kim on 2019.04.29
 */
public class PowerballParser extends TimerTask {

    private int lastPageNum = 1;
    private int startPageNum = 1;

    private Logger logger = LoggerFactory.getLogger(PowerballParser.class);

    private String userAgent = "Mozilla/5.0 (Linux; U; Android 4.3; en-us; SM-N900T Build/JSS15J) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";

    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    private PowerballDAO powerballDAO = new PowerballDAO();

    private SimpleDateFormat korDateFormat = null;

    MessageQueueProducer messageQueueProducer = new MessageQueueProducer();
    ObjectMapper mapper = new ObjectMapper();

    private Map<String, String> loginCookies = new HashMap<>();
    private BasicCookieStore cookieStore = null;

    private WebCrawlingService crawlingService = null;

    private boolean isLogin = false;

    public PowerballParser(String date) {
        crawlingService = new WebCrawlingService();
        mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);

        korDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        korDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

        loginSite();

        //최초 실행시 오늘  2페이지 데이타를 가져온다. 대략 1시간
        //현재시간을 확인하여, 최대 페이지수를 계산한다.
        //String date = korDateFormat.format(new Date());
        if (date != null) {
            saveDayData(date);
        }
    }

    public void loginSite() {

        Map<String, String> data = new HashMap<>();
        //data.put("userId", "wjdqjrdl");
        //data.put("password", "yx7663w2");
        //data.put("userId", "immerong");
        //data.put("password", "k9400275!@");

        String userId = Configuration.getPowerballLoginId();
        String password = Configuration.getPowerballLoginPw();

        if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(password)) {
            logger.error("login info fail. userId={}, password={}", userId, password);
            logger.error("must be settings .env", userId, password);

            System.err.println(String.format("login info fail. userId=%s, password=%s", userId, password));
            System.err.println("must be settings .env");
            System.exit(0);
        }

        data.put("userId", userId);
        data.put("password", password);
        data.put("rememberLoginId", "");
        data.put("newsEventYn", "");
        data.put("checkSave", "on");

        try {
            this.cookieStore = new WebCrawlingService()
                    .loginParams(data)
                    .loginUrl("https://www.dhlottery.co.kr/userSsl.do?method=login")
                    .referer("https://www.dhlottery.co.kr")
                    .login();

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void run() {

        int second600 = (int) (System.currentTimeMillis() / 1000) % 600; //10분 단위 초

        String minutesecond = (int) Math.floor(second600 / 60) + ":" + (second600 % 60);
        LocalTime now = LocalTime.now();
        int minute1440 = now.getHour() * 60 + now.getMinute(); //하루 24시간 기준 minute 0 ~ 1440

        logger.info("minute1440=" + minute1440 + ", second600=" + second600 + ", time=" + minutesecond);

        //과도한 트래픽을 발생시켜 차단당할수 있음으로, 불피요한 트래픽을 만들어서는 안됨
        //서버의 사간값을 항상 잘 맞춰두어야 함.
        //앞뒤로 1-2초정도 여유를 두어 호출해야함.
        //파워볼은 00시02분부터 시작 하여 23시57분까지 288회차 진행
        //결과 시간은 2분50초 ~ 3분00초, 7분50초 8분00초 (평균 53초에 결과 나옴)

        //1. 2분52초 ~ 2분55초 까지 게임결과 파싱처리,
        //2. 7분52초 ~ 7분55초 까지 게임결과 파싱처리,
        //todo 구간안에 결과값을 가져오지 못한다면, telegram 알림처리 필요

        try {
            if (second600 >= (2 * 60 + 53) && second600 <= (2 * 60 + 59)) {
                realtimeCrawing();
            }

            if (second600 >= (3 * 60 + 5) && second600 <= (3 * 60 + 6)) {
                realtimeCrawing(); //3분 5초에 한번 실행
            }

            if (second600 >= (3 * 60 + 15) && second600 <= (3 * 60 + 16)) {
                realtimeCrawing(); //3분 15초에 한번 실행
            }

            //7분55초 ~ 8분00초 까지 게임결과 파싱처리
            if (second600 >= (7 * 60 + 53) && second600 <= (7 * 60 + 59)) {
                realtimeCrawing();
            }

            if (second600 >= (8 * 60 + 5) && second600 <= (8 * 60 + 6)) {
                realtimeCrawing(); //8분 5초에 한번 실행
            }

            if (second600 >= (8 * 60 + 15) && second600 <= (8 * 60 + 16)) {
                realtimeCrawing(); //8분 15초에 한번 실행
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    //실시간 크롤링 처리
    public void realtimeCrawing() {

        String date = korDateFormat.format(new Date());
        try {
            List<Powerball> powerballList = parsing(date, 1, true);
            if (powerballList.size() > 0) {

                Powerball powerballData = powerballList.get(0);
                int round = powerballDAO.getTodayRoundByTotalRound(powerballData.getDate(), powerballData.getTotal_round());
                powerballData.setRound(round);

                boolean is_insert = powerballDAO.existCheckAndInsertPowerball(powerballData);
                logger.info("is_insert : {}, powerballData={}", is_insert, powerballData);
                if (is_insert) {
                    PushMessageService.sendPowerballMessage(powerballData);
                }
            }
        } catch(Exception ex) {
            new TelegramMessageService().sendMessage("[" + Configuration.getServerInstance() + "] 파워볼 오류 : " + ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
    }

    public List<Powerball> parsing(String date, int page, boolean onlyTopRow) throws Exception {

        String time = System.currentTimeMillis() + "";

        String gameRoundResultUrl = "https://dhlottery.co.kr/gameInfo.do?method=powerWinNoList";
        //String valueString  = stringGetter.getString(gameRoundScheduleUrl);

        Map<String, String> param = new HashMap<>();
        param.put("nowPage", page + "");
        param.put("searchDate", StringUtils.replace(date, "-", ""));
        param.put("calendar", date);
        param.put("sortType", "num");

        logger.info("parsing date={}, page={}, onlyTopRow={}", date, page, onlyTopRow);

        Connection.Response response = null;
        Document doc = null;
        String valueString = "";
        /*
        response = Jsoup.connect(gameRoundResultUrl)
                .header("Origin", "https://www.dhlottery.co.kr")
                .header("referer", "https://dhlottery.co.kr/gameInfo.do?method=powerWinNoList")
                .userAgent(userAgent)
                .timeout(3000)
                .data(param)
                .method(Connection.Method.POST)
                .cookies(loginCookies)
                .execute();

        doc = response.parse();
        valueString = doc.html();

         */
        WebCrawlingService crawlingService =  new WebCrawlingService()
                .targetUrl("https://dhlottery.co.kr/gameInfo.do?method=powerWinNoList")
                .referer("https://www.dhlottery.co.kr");

        valueString = crawlingService
                .cookieStore(cookieStore)
                .postData(param)
                .execute();

        if (valueString.contains("dhlottery.co.kr/user.do?method=login")) {
            this.loginSite();
            /*
            response = Jsoup.connect(gameRoundResultUrl)
                    .header("Origin", "https://www.dhlottery.co.kr")
                    .header("referer", "https://dhlottery.co.kr/gameInfo.do?method=powerWinNoList")
                    .userAgent(userAgent)
                    .timeout(3000)
                    .data(param)
                    .method(Connection.Method.POST)
                    .cookies(loginCookies)
                    .execute();

            doc = response.parse();
            valueString = doc.html();

             */

            valueString = crawlingService
                    .cookieStore(cookieStore)
                    .postData(param)
                    .execute();
        }

        int responseCode = crawlingService.getResponseCode();
        String responseMessage = crawlingService.getResponsePhrase();
        long responseTime = crawlingService.getResponseTime();

        addParsingLog("powerball", gameRoundResultUrl, responseCode, responseMessage, responseTime);

        List<Powerball> powerballList = getPowerballRowList(valueString, onlyTopRow);

        //최근회차가 list 앞에 위차함으로 역순으로 처리한다.
        Collections.reverse(powerballList);

        return powerballList;

    }

    public boolean addPowerBallData(Powerball powerballData) {
        int round = powerballDAO.getTodayRoundByTotalRound(powerballData.getDate(), powerballData.getTotal_round());
        powerballData.setRound(round);
        boolean is_update = powerballDAO.existCheckAndInsertPowerball(powerballData);
        return is_update;
    }


    public List<Powerball> getPowerballRowList(String valueString, boolean onlyTopRow) {
        Document document = Jsoup.parse(valueString);
        List<HashMap<String, String>> resultHashMapList = new ArrayList<HashMap<String, String>>();

        Element table = document.select(".tbl_data_col").get(0);

        Elements trElements = table.select("tbody > tr");

        ArrayList<Powerball> powerballDataRows = new ArrayList<>(trElements.size());

        for (Element ele : trElements) {
            //System.out.println(ele.html());

            HashMap<String, String> gameMap = new HashMap();

            try {

                // logger.info(ele.html());

                String date = StringUtils.trimToEmpty(ele.select("td:eq(0)").text()); //추첨일(YYYY-MM-DD)
                String total_round = StringUtils.trimToEmpty(ele.select("td:eq(1)").text()); //전체 회차
                String winningNumberText = ele.select("td:eq(2)").html(); //당첨번호 텍스트..
                String powerball = StringUtils.trimToEmpty(ele.select("td:eq(3)").text()); //파워볼
                String sum_numbers = StringUtils.trimToEmpty(ele.select("td:eq(4)").text()); //숫자합
                String oe_text = StringUtils.trimToEmpty(ele.select("td:eq(5)").text()); //홀/
                String size_text = StringUtils.trimToEmpty(ele.select("td:eq(6)").text()); //대/중/
                String sum_section_text = StringUtils.trimToEmpty(ele.select("td:eq(7)").text()); //숫자합구
                String powerball_section_text = StringUtils.trimToEmpty(ele.select("td:eq(8)").text()); //파워볼구간


                String numbers = getWinningBallNumbers(winningNumberText);
                String oe = "";
                String ou = "";
                String powerball_oe = "";
                String powerball_ou = "";
                String size = "";
                String section = "";
                String powerball_section = "";
                int sum_num = 0;
                int pow_num = 0;

                try {
                    sum_num = Integer.parseInt(sum_numbers);
                    pow_num = Integer.parseInt(powerball);
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                    logger.info("tr ele={}", ele);
                    logger.info("winningNumberText={}, numbers={}, sum_numbers={}, powerball={}", winningNumberText, numbers, sum_numbers, powerball);
                    //System.out.println(valueString);
                    throw ex;
                }

                oe = sum_num % 2 == 0 ? "E" : "O";
                powerball_oe = pow_num % 2 == 0 ? "E" : "O";
                ou = sum_num * 10 > 725 ? "O" : "U";
                powerball_ou = pow_num * 10 > 45 ? "O" : "U";


                if (sum_num >= 15 && sum_num <= 64) { //15 ~ 64 소
                    size = "S";
                } else if (sum_num >= 65 && sum_num <= 80) { //65 ~ 80 중
                    size = "M";
                } else if (sum_num >= 81 && sum_num <= 130) { //81 ~ 130 대
                    size = "L";
                }

                section = sum_section_text.replaceAll("[^A-F]", "");
                powerball_section = powerball_section_text.replaceAll("[^A-F]", "");

                Powerball powerballData = new Powerball();
                powerballData.setDate(date);
                powerballData.setStatus("D");
                powerballData.setTotal_round(Integer.parseInt(total_round));
                powerballData.setBalls(numbers);
                powerballData.setPowerball(powerball);
                powerballData.setSum_numbers(sum_numbers);
                powerballData.setSize(size);
                powerballData.setOe(oe);
                powerballData.setPower_oe(powerball_oe);
                powerballData.setOu(ou);
                powerballData.setPower_ou(powerball_ou);
                powerballData.setSection(section);
                powerballData.setPower_section(powerball_section);

                powerballDataRows.add(powerballData);

                logger.info("date={}, total_round={}, round={}, numbers={}, oe={}, ou={}, power_oe={}, power_ou={}, size={}",
                        powerballData.getDate(),
                        powerballData.getTotal_round(),
                        powerballData.getRound(),
                        powerballData.getBalls(),
                        powerballData.getOe(),
                        powerballData.getOu(),
                        powerballData.getPower_oe(),
                        powerballData.getPower_ou(),
                        powerballData.getSize()
                );
                if (onlyTopRow) break; //첫줄만 가져오는 경우
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                logger.error(ele.toString());

            }
        }

        return powerballDataRows;
    }

    /**
     * 정규식으로 당첨번호를 추출한다.
     * 추출된 당첨번호는 오름차순으로 정렬
     *
     * @param text
     * @return 5개의 2자리 숫자를 ,(콤마로) 구분하여 리턴. 숫자가 10보다 작으면 앞에 0을 추가
     */
    private String getWinningBallNumbers(String text) {
        Pattern pattern_date = Pattern.compile("'([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})'");
        Matcher matcher = pattern_date.matcher(text);

        ArrayList<Integer> numbers = new ArrayList<>();
        ArrayList<String> winningNumbers = new ArrayList<>();

        if (matcher.find()) {
            logger.info(String.format("match  : %s, numbers=%s,%s,%s,%s,%s", matcher.group(0), matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4), matcher.group(5)));

            numbers.add(Integer.parseInt(matcher.group(1)));
            numbers.add(Integer.parseInt(matcher.group(2)));
            numbers.add(Integer.parseInt(matcher.group(3)));
            numbers.add(Integer.parseInt(matcher.group(4)));
            numbers.add(Integer.parseInt(matcher.group(5)));

            //Collections.sort(numbers); //소팅하지 않는다.
            numbers.forEach((number) -> {
                winningNumbers.add(String.format("%02d", number));
            });

            String result = StringUtils.join(winningNumbers, ",");

            logger.debug(text + "=> " + result);

            return result;
        } else {

        }

        return "";
    }


    private void addParsingLog(String game_type, String url, int statusCode, String statusMessage, long responseTime) {

        powerballDAO.insertCrawlingLog(
                game_type
                , url
                , statusCode
                , statusMessage
                , responseTime
        );
    }


    public boolean webhook() {
        String webook_target_url = "";
        return true;
    }

    public boolean eventCall() {

        return true;
    }


    /**
     * 최근 날짜 데이타를 조회하여 데이타를 입력한다.
     */
    public void saveRecent365DayData() {

        int beforeDay = 365;

        for (int i = beforeDay; i >= 1; i--) {
            LocalDate targetDay = LocalDate.now().minusDays(i);
            String date = targetDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            saveDayData(date);
            try {
                Thread.sleep(2000L);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * 지정된 날짜의 모든 파워볼 결과값을 저장한다.
     *
     * @param date
     * @return
     */
    public int saveDayData(String date) {
        int count = 0;

        for (int page = 29; page >= 1; page--) {
            try {
                List<Powerball> powerballList = parsing(date, page, false);
                for (Powerball powerballData : powerballList) {
                    addPowerBallData(powerballData);
                    count++;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                logger.error(ex.getMessage(), ex);
            }

        }

        return count;

    }

    /**
     * 지정한 시작일과 종료일에 맞춰 파싱한다.
     */
    public void saveDayData(String startDate, String endDate) {

        int todays = 2000;

        LocalDate startLocalDate = LocalDate.parse(startDate);
        for (int i = 0; i < todays; i++) {
            LocalDate targetDay = startLocalDate.plusDays(i);
            String parseDate = targetDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

            saveDayData(parseDate);

            try {
                Thread.sleep(2000L);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            if (parseDate.equals(endDate)) {
                break;
            }
        }
    }

    public static void main(String[] args) {
        SimpleDateFormat korDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        korDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

        String today = korDateFormat.format(new Date());

        if (args.length < 1) {
            System.err.println("Usage: java -cp=./powerball_parser.jar PowerballParser 2020-01-26");
            System.exit(0);
        } else if (args.length == 1) {

            String[] dates = args[0].split(",");
            PowerballParser parser = new PowerballParser(dates[0]);
            for (int i = 1; i < dates.length; i++) {
                parser.saveDayData(dates[i].trim());
            }

        } else if (args.length == 2) {
            PowerballParser parser = new PowerballParser(args[0]);
            parser.saveDayData(args[0], args[1]);
        } else {
            //System.err.println("argument invalid");

            PowerballParser powerballParser = new PowerballParser(today);
        }


        if (true) return;


        //PowerballParser parser = new PowerballParser(null);
        //PowerballParser parser = new PowerballParser(today);
        //PowerballParser parser = new PowerballParser("2020-01-26");

        //parser.saveDayData("2015-01-01", "2018-04-28");
        //parser.saveDayData("2020-01-26");
        //parser.saveRecent365DayData();
    }
}
