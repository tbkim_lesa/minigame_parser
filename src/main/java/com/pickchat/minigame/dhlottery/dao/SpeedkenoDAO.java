package com.pickchat.minigame.dhlottery.dao;

import com.pickchat.minigame.dhlottery.bean.Speedkeno;
import com.pickchat.minigame.dhlottery.db.SqlSessionManager;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

public class SpeedkenoDAO {

    // Logger
    private Logger logger;
    // Session Factory

    private SqlSessionFactory mSqlSessionFactory = SqlSessionManager.getSqlSession();

    // Domain
    private static final String DOMAIN = "speedkeno.";


    public SpeedkenoDAO() {
        logger = LoggerFactory.getLogger(SpeedkenoDAO.class);
    }


    public HashMap<String, Object> getSpeedkeno(String date, String round) {
        SqlSession session = mSqlSessionFactory.openSession(true);
        try {
            HashMap<String, String> param = new HashMap<>();
            param.put("date", date);
            param.put("round", round);
            return session.selectOne(DOMAIN + "selectSpeedkeno", param);

        } catch (Exception e) {
            logger.warn(e.getMessage(), e);
        } finally {
            if (session != null) session.close();
        }
        return null;
    }

    public Integer getTodayMaxRound(String date) {
        SqlSession session = mSqlSessionFactory.openSession(true);
        try {
            return session.selectOne(DOMAIN + "selectTodayMaxRound", date);
        } catch (Exception e) {
            logger.warn(e.getMessage(), e);
        } finally {
            if (session != null) session.close();
        }
        return null;
    }

    /**
     * 스피드키노 게임결과를 insert
     * @param speedkeno 스피드키노 데이타
     * @return insert 되었다면 true, 그외에는 false;
     */
    public boolean existCheckAndInsertSpeedkeno(Speedkeno speedkeno) {
        SqlSession session = mSqlSessionFactory.openSession(true);
        try {
            //기존에 값이 insert 되어 있는지 확인한다.
            Speedkeno speedkenoData = session.selectOne(DOMAIN + "selectSpeedkenoByTotalRound", speedkeno.getTotal_round());
            if(speedkenoData != null) return false;
            speedkenoData = session.selectOne(DOMAIN + "selectSpeedkeno", speedkeno);

            if(speedkenoData != null) return false;

            return session.update(DOMAIN + "insertSpeedkenoData", speedkeno) > 0;
        } catch (Exception e) {
            logger.warn(e.getMessage(), e);
        } finally {
            if (session != null) session.close();
        }

        return false;

    }

    public boolean insertCrawlingLog(
            String game_type, String parsing_url, int response_status, String response_body, long response_time
    ) {

        SqlSession session = mSqlSessionFactory.openSession(true);
        try {

            HashMap<String, String> params = new HashMap<>();
            params.put("game_type", game_type);
            params.put("parsing_url", parsing_url);
            params.put("response_status", response_status + "");
            params.put("response_body", response_body);
            params.put("response_time", response_time + "");

            session.insert(DOMAIN + "insertSpeedkenoCrawingLog", params);

        } catch (Exception e) {
            logger.warn(e.getMessage(), e);
        } finally {
            if (session != null) session.close();
        }
        return false;
    }

    public int insertGameEvent(
            String game_type, String event_type, String date, String round, String hook_url
    ) {
        SqlSession session = mSqlSessionFactory.openSession(true);
        try {
            HashMap<String, String> params = new HashMap<>();
            params.put("game_type", game_type);
            params.put("event_type", event_type);
            params.put("date", date);
            params.put("round", round);
            params.put("hook_url", hook_url);

            int insert_id = session.insert(DOMAIN + "insertSpeedkenoGameEvent", params);

            return insert_id;
        } catch (Exception e) {
            logger.warn(e.getMessage(), e);
        } finally {
            if (session != null) session.close();
        }

        return -1;

    }

    //todo 공통부분은 common.sqlmap.xml + commonDAO로 처리하도록 수정
    public boolean deleteCrawlingLog() {
        SqlSession session = mSqlSessionFactory.openSession(true);
        try {
            session.delete(DOMAIN + "deletSpeedkenoCrawlingLog");

        } catch (Exception e) {
            logger.warn(e.getMessage(), e);
        } finally {
            if (session != null) session.close();
        }

        return false;
    }



}
