package com.pickchat.minigame.dhlottery.dao;

import com.pickchat.minigame.dhlottery.bean.Powerball;
import com.pickchat.minigame.dhlottery.db.SqlSessionManager;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.HashSet;

public class PowerballDAO {

    // Logger
    private Logger logger;
    // Session Factory

    private SqlSessionFactory mSqlSessionFactory = SqlSessionManager.getSqlSession();
    // Domain
    private static final String DOMAIN = "powerball.";


    public PowerballDAO() {
        logger = LoggerFactory.getLogger(PowerballDAO.class);
    }


    public HashMap<String, Object> getPowerball(String date, String round) {
        SqlSession session = mSqlSessionFactory.openSession(true);
        try {
            HashMap<String, String> param = new HashMap<>();
            param.put("date", date);
            param.put("round", round);
            return session.selectOne(DOMAIN + "selectPowerballLadder", param);

        } catch (Exception e) {
            logger.warn(e.getMessage(), e);
        } finally {
            if (session != null) session.close();
        }
        return null;
    }

    public Integer getTodayMaxRound(String date) {
        SqlSession session = mSqlSessionFactory.openSession(true);
        try {
            return session.selectOne(DOMAIN + "selectTodayMaxRound", date);

        } catch (Exception e) {
            logger.warn(e.getMessage(), e);
        } finally {
            if (session != null) session.close();
        }
        return null;
    }

    /**
     * 파워볼 게임결과를 insert
     * @param powerball 파워볼 데이타
     * @return insert 되었다면 true, 그외에는 false;
     */
    public boolean existCheckAndInsertPowerball(Powerball powerball) {
        SqlSession session = mSqlSessionFactory.openSession(true);
        try {
            //기존에 값이 insert 되어 있는지 확인한다.
            Powerball powerballData = session.selectOne(DOMAIN + "selectPowerballByTotalRound", powerball.getTotal_round());

            logger.info("existCheckAndInsertPowerball {}", powerballData);
            if(powerballData != null) {
                logger.info("data exist!!  update ");
                session.update(DOMAIN + "updatePowerballDataResult", powerball);
                return false;
            }
            return session.update(DOMAIN + "insertPowerballData", powerball) > 0;
        } catch (Exception e) {
            logger.warn(e.getMessage(), e);
        } finally {
            if (session != null) session.close();
        }

        return false;

    }


    public Powerball selectPowerballByTotalRound(int total_round) {
        SqlSession session = mSqlSessionFactory.openSession(true);
        try {
            //기존에 값이 insert 되어 있는지 확인한다.
           return session.selectOne(DOMAIN + "selectPowerballByTotalRound", total_round);

        } catch (Exception e) {
            logger.warn(e.getMessage(), e);
        } finally {
            if (session != null) session.close();
        }

        return null;
    }


    public boolean insertCrawlingLog(
            String game_type, String parsing_url, int response_status, String response_body, long response_time
    ) {

        SqlSession session = mSqlSessionFactory.openSession(true);
        try {

            HashMap<String, String> params = new HashMap<>();
            params.put("game_type", game_type);
            params.put("parsing_url", parsing_url);
            params.put("response_status", response_status + "");
            params.put("response_body", response_body);
            params.put("response_time", response_time + "");

            session.insert(DOMAIN + "insertPowerballCrawingLog", params);

        } catch (Exception e) {
            logger.warn(e.getMessage(), e);
        } finally {
            if (session != null) session.close();
        }

        return false;

    }

    public int insertGameEvent(
            String game_type, String event_type, String date, String round, String hook_url
    ) {
        SqlSession session = mSqlSessionFactory.openSession(true);
        try {
            HashMap<String, String> params = new HashMap<>();
            params.put("game_type", game_type);
            params.put("event_type", event_type);
            params.put("date", date);
            params.put("round", round);
            params.put("hook_url", hook_url);

            int insert_id = session.insert(DOMAIN + "insertPowerballGameEvent", params);

            return insert_id;
        } catch (Exception e) {
            logger.warn(e.getMessage(), e);
        } finally {
            if (session != null) session.close();
        }

        return -1;

    }

    public boolean deleteCrawlingLog() {
        SqlSession session = mSqlSessionFactory.openSession(true);
        try {
            session.delete(DOMAIN + "deletPowerballCrawlingLog");

        } catch (Exception e) {
            logger.warn(e.getMessage(), e);
        } finally {
            if (session != null) session.close();
        }

        return false;
    }

    public int getTodayRoundByTotalRound(String date, int totalRound) {
        int round = 0;
        Powerball powerballData = this.selectPowerballByTotalRound(totalRound - 1);
        if (powerballData != null) {
            int prevRound = powerballData.getRound();
            round = (prevRound % 288) + 1;
        } else {
            int maxRound = this.getTodayMaxRound(date);
            round = maxRound + 1;
        }
        return round;
    }




}
