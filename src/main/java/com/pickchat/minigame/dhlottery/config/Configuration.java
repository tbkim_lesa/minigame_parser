package com.pickchat.minigame.dhlottery.config;


import io.github.cdimascio.dotenv.Dotenv;

/**
 * mybatis db session을 runtime에서 바꾸기 위해 설정 정보를 저장한다.
 */
public class Configuration {
    //public static Logger logger = LoggerFactory.getLogger(Configuration.class);
    private static String server_instance = "";
    private static String springProfilesActive = "";
    private static String mLoggerName = "com.pickchat.minigame";
    private static  String siteLoginId = "";
    private static  String siteLoginPw = "";

    static {
        server_instance = (System.getProperty("server_instance") != null) ? System.getProperty("server_instance") : "local";
        springProfilesActive = (System.getProperty("spring.profiles.active") != null) ? System.getProperty("spring.profiles.active") : "local";
        Dotenv dotenv = Dotenv.configure()
                .ignoreIfMalformed()
                .ignoreIfMissing()
                .load();

        siteLoginId  = dotenv.get("POWERBALL_LOGIN_ID");
        siteLoginPw  = dotenv.get("POWERBALL_LOGIN_PW");

        System.out.println(".env loading..");
        System.out.println(".env siteLoginId = " + siteLoginId);
        System.out.println(".env siteLoginPw = " + siteLoginPw);
    }

    public static boolean is_development() {
        if ("dev".equals(server_instance)) return true;
        return false;
    }

    public static boolean is_beta() {
        if ("beta".equals(server_instance)) return true;
        return false;
    }


    public static boolean is_production() {
        if ("prd".equals(server_instance)) return true;
        return false;
    }

    public static String getServerInstance() {
        return Configuration.server_instance;
    }

    public static String getSpringProfilesActive(){
        return Configuration.springProfilesActive;
    }

    public static String getPowerballLoginId() {
        return siteLoginId;
    }

    public static String getPowerballLoginPw() {
        return siteLoginPw;
    }

    public static void setLoggerName(String loggerName) {
        mLoggerName = loggerName;
    }

    public static String getLoggerName(){
        return mLoggerName;
    }

    public static void setServer_instance(String server_instance) {
        Configuration.server_instance = server_instance;
    }
}
