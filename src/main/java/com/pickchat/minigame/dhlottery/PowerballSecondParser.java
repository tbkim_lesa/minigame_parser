package com.pickchat.minigame.dhlottery;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pickchat.minigame.crawler.service.PushMessageService;
import com.pickchat.minigame.crawler.service.TelegramMessageService;
import com.pickchat.minigame.crawler.service.WebCrawlingService;
import com.pickchat.minigame.dhlottery.bean.Powerball;
import com.pickchat.minigame.dhlottery.config.Configuration;
import com.pickchat.minigame.dhlottery.dao.PowerballDAO;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.impl.client.BasicCookieStore;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PowerballSecondParser extends TimerTask {

    private static Logger logger = LoggerFactory.getLogger(PowerballSecondParser.class);

    private String userAgent = "Mozilla/5.0 (Linux; U; Android 4.3; en-us; SM-N900T Build/JSS15J) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";

    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    private PowerballDAO powerballDAO = new PowerballDAO();

    private SimpleDateFormat korDateFormat = null;

    ObjectMapper mapper = new ObjectMapper();

    private Map<String, String> loginCookies = new HashMap<>();
    private BasicCookieStore cookieStore = null;

    private WebCrawlingService crawlingService = null;

    private boolean isLogin = false;

    public PowerballSecondParser() {
        crawlingService = new WebCrawlingService();
        mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
        try {
            todayCrawing();
        } catch(Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void run() {

        int second600 = (int) (System.currentTimeMillis() / 1000) % 600; //10분 단위 초

        String minutesecond = (int) Math.floor(second600 / 60) + ":" + (second600 % 60);
        LocalTime now = LocalTime.now();
        int minute1440 = now.getHour() * 60 + now.getMinute(); //하루 24시간 기준 minute 0 ~ 1440
        logger.info("minute1440=" + minute1440 + ", second600=" + second600 + ", time=" + minutesecond);
        try {

            if (second600 >= (2 * 60 + 50) && second600 <= (2 * 60 + 53)) {
                realtimeCrawing(); //3분 00초에 한번 실행
            }
            if (second600 >= (3 * 60) && second600 <= (3 * 60 + 1)) {
                realtimeCrawing(); //3분 00초에 한번 실행
            }

            if (second600 >= (3 * 60 + 5) && second600 <= (3 * 60 + 6)) {
                realtimeCrawing(); //3분 5초에 한번 실행
            }

            if (second600 >= (3 * 60 + 10) && second600 <= (3 * 60 + 11)) {
                realtimeCrawing(); //3분 10초에 한번 실행
            }

            if (second600 >= (3 * 60 + 10) && second600 <= (3 * 60 + 11)) {
                realtimeCrawing(); //3분 10초에 한번 실행
            }

            if (second600 >= (7 * 60 + 50) && second600 <= (7 * 60 + 53)) {
                realtimeCrawing(); //3분 00초에 한번 실행
            }

            if (second600 >= (8 * 60) && second600 <= (8 * 60 + 1)) {
                realtimeCrawing(); //8분 15초에 한번 실행
            }

            if (second600 >= (8 * 60 + 5) && second600 <= (8 * 60 + 6)) {
                realtimeCrawing(); //8분 5초에 한번 실행
            }

            if (second600 >= (8 * 60 + 10) && second600 <= (8 * 60 + 11)) {
                realtimeCrawing(); //8분 10초에 한번 실행
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    //실시간 크롤링 처리
    public void realtimeCrawing() {
        try {
            /*
            Powerball powerballData = this.parsing();
            int todayMaxRound = powerballDAO.getTodayMaxRound(powerballData.getDate());

            if(todayMaxRound <  powerballData.getRound()) { //마지막 입력된 회차가 파싱 회차보다 작은경우, 파싱된 데이타를 입력
                boolean is_insert = powerballDAO.existCheckAndInsertPowerball(powerballData);
                logger.info("is_insert : {}, powerballData={}", is_insert, powerballData);
                if (is_insert) {
                    PushMessageService.sendPowerballMessage(powerballData);
                    String telegramMessage = String.format("[%s] 파워볼[ntry] 데이타 입력. 전체회차=%s, 당일회차=%s ", Configuration.getServerInstance(), powerballData.getRound(), powerballData.getTotal_round());
                    new TelegramMessageService().sendMessage(telegramMessage);
                }
            }
             */

            this.parsing();

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void todayCrawing() throws Exception {

        String time = System.currentTimeMillis() + "";

        String powerballResultJson = "http://ntry.com/stats/api.php";
        //String valueString  = stringGetter.getString(gameRoundScheduleUrl);

        Map<String, String> param = new HashMap<>();
        param.put("offset", "0");
        param.put("size", "288");
        param.put("cmd", "get_round_data");
        param.put("type", "powerball");
        param.put("get_type", "round");
        param.put("get_value", "288");

        Connection.Response response = null;
        Document doc = null;
        String valueString = "";

        WebCrawlingService crawlingService = new WebCrawlingService()
                .targetUrl(powerballResultJson)
                .referer("http://ntry.com/scores/powerball/main.php");

        valueString = crawlingService
                .postData(param)
                .execute();

        logger.info("json : {}", valueString);
        int responseCode = crawlingService.getResponseCode();
        String responseMessage = crawlingService.getResponsePhrase();
        long responseTime = crawlingService.getResponseTime();


        HashMap<String, ?> powerballDayResult =  mapper.readValue(valueString, HashMap.class);
        String response_code = String.valueOf(powerballDayResult.get("code"));
        ArrayList<HashMap> roundList = (ArrayList<HashMap>)powerballDayResult.get("data");

        ArrayList<Powerball> dayPowballList = new ArrayList<>();

        String date = "";
        for (HashMap powerballResult : roundList) {
            date = String.valueOf(powerballResult.get("reg_date"));
            String total_round = String.valueOf(powerballResult.get("round"));
            String round =  String.valueOf(powerballResult.get("date_round"));
            String size =  String.valueOf(powerballResult.get("sum_size_alias"));
            String oe =  String.valueOf(powerballResult.get("sum_odd_even_alias"));
            String ou =  String.valueOf(powerballResult.get("sum_under_over_alias"));
            String section =  String.valueOf(powerballResult.get("sum_section"));
            String powerball = String.valueOf(powerballResult.get("powerball"));
            String powerball_oe =  String.valueOf(powerballResult.get("powerball_odd_even_alias"));
            String powerball_ou =  String.valueOf(powerballResult.get("powerball_under_over_alias"));
            String powerball_section =  String.valueOf(powerballResult.get("powerball_section"));
            String sum_numbers =  String.valueOf(powerballResult.get("sum"));

            String[] balls = new String[5];
            balls[0] = String.format("%02d",  Integer.parseInt((String)powerballResult.get("ball_1")));
            balls[1] = String.format("%02d",  Integer.parseInt((String)powerballResult.get("ball_2")));
            balls[2] = String.format("%02d",  Integer.parseInt((String)powerballResult.get("ball_3")));
            balls[3] = String.format("%02d",  Integer.parseInt((String)powerballResult.get("ball_4")));
            balls[4] = String.format("%02d",  Integer.parseInt((String)powerballResult.get("ball_5")));


            oe = StringUtils.replaceEachRepeatedly(oe, new String[]{"홀", "짝" }, new String[]{"O", "E"});
            ou = StringUtils.replaceEachRepeatedly(ou, new String[]{"언더", "오버" }, new String[]{"U", "O"});
            powerball_oe = StringUtils.replaceEachRepeatedly(powerball_oe, new String[]{"홀", "짝" }, new String[]{"O", "E"});
            powerball_ou = StringUtils.replaceEachRepeatedly(powerball_ou, new String[]{"언더", "오버" }, new String[]{"U", "O"});
            size = StringUtils.replaceEachRepeatedly(size, new String[]{"대", "중","소" }, new String[]{"L", "M", "S"});

            String numbers = StringUtils.join(balls, ",");

            Powerball powerballData = new Powerball();
            powerballData.setDate(date);
            powerballData.setRound(Integer.parseInt(round));
            powerballData.setStatus("D");
            powerballData.setTotal_round(Integer.parseInt(total_round));
            powerballData.setBalls(numbers);
            powerballData.setPowerball(powerball);
            powerballData.setSum_numbers(String.valueOf(sum_numbers));
            powerballData.setSize(size);
            powerballData.setOe(oe);
            powerballData.setPower_oe(powerball_oe);
            powerballData.setOu(ou);
            powerballData.setPower_ou(powerball_ou);
            powerballData.setSection(section);
            powerballData.setPower_section(powerball_section);

            dayPowballList.add(powerballData);

        }

        dayPowballList.forEach(powerballData -> {
            powerballDAO.existCheckAndInsertPowerball(powerballData);
        });

            String telegramMessage = String.format("[%s] %s일 파워볼[ntry]  전체 데이타 입력, 총=%s회차 ", Configuration.getServerInstance(), date, dayPowballList.size());
            new TelegramMessageService().sendMessage(telegramMessage);
    }

    /**
     * ntry powerball 최근 데이타를 파싱 처리한다.
     * result json 구조
     * {
     *   "date": "2020-09-14",
     *   "times": "1029342",
     *   "date_round": 213,
     *   "ball": [5, 24, 8, 21, 10, "9"],
     *   "pow_ball_oe": "짝",
     *   "pow_ball_unover": "언더",
     *   "def_ball_sum": "68",
     *   "def_ball_oe": "홀",
     *   "def_ball_unover": "오버",
     *   "def_ball_size": "소",
     *   "def_ball_section": "E"
     * }
     *
     * @return
     * @throws Exception
     */
    public void parsing() throws Exception {

        String time = System.currentTimeMillis() + "";

        String powerballResultJson = "http://ntry.com/data/json/games/powerball/result.json";
        //String valueString  = stringGetter.getString(gameRoundScheduleUrl);

        Map<String, String> param = new HashMap<>();

        Connection.Response response = null;
        Document doc = null;
        String valueString = "";

        WebCrawlingService crawlingService = new WebCrawlingService()
                .targetUrl(powerballResultJson)
                .referer("http://ntry.com/scores/powerball/main.php");

        valueString = crawlingService
                .postData(param)
                .execute();

        logger.info("json : {}", valueString);
        int responseCode = crawlingService.getResponseCode();
        String responseMessage = crawlingService.getResponsePhrase();
        long responseTime = crawlingService.getResponseTime();

        HashMap<String, ?> powerballResult =  mapper.readValue(valueString, HashMap.class);

        String date = String.valueOf(powerballResult.get("date"));
        String total_round = String.valueOf(powerballResult.get("times"));
        String round =  String.valueOf(powerballResult.get("date_round"));
        String size =  String.valueOf(powerballResult.get("def_ball_size"));
        String oe =  String.valueOf(powerballResult.get("def_ball_oe"));
        String ou =  String.valueOf(powerballResult.get("def_ball_unover"));
        String section =  String.valueOf(powerballResult.get("def_ball_section"));
        String powerball_oe =  String.valueOf(powerballResult.get("pow_ball_oe"));
        String powerball_ou =  String.valueOf(powerballResult.get("pow_ball_unover"));
        String powerball_section =  "";
        String powerball = "";
        final ArrayList<?> ballList = (ArrayList<Object>)powerballResult.get("ball");
        String[] balls = new String[5];

        int sum_numbers = 0;

        for(int i = 0; i < ballList.size();i++) {
            if(i < 5) {
                balls[i] = String.format("%02d", ballList.get(i));
                sum_numbers += (Integer)ballList.get(i);
            } else if(i == 5) {
                powerball = String.valueOf(ballList.get(i));
            }
        }

        if(powerball.equals("0") || powerball.equals("1") || powerball.equals("2")) {
            powerball_section = "A";
        } else if(powerball.equals("3") || powerball.equals("4") ) {
            powerball_section = "B";
        } else if(powerball.equals("5") || powerball.equals("6") ) {
            powerball_section = "C";
        } else if(powerball.equals("7") || powerball.equals("8") || powerball.equals("9") )  {
            powerball_section = "D";
        }

        oe = StringUtils.replaceEachRepeatedly(oe, new String[]{"홀", "짝" }, new String[]{"O", "E"});
        ou = StringUtils.replaceEachRepeatedly(ou, new String[]{"언더", "오버" }, new String[]{"U", "O"});
        powerball_oe = StringUtils.replaceEachRepeatedly(powerball_oe, new String[]{"홀", "짝" }, new String[]{"O", "E"});
        powerball_ou = StringUtils.replaceEachRepeatedly(powerball_ou, new String[]{"언더", "오버" }, new String[]{"U", "O"});
        size = StringUtils.replaceEachRepeatedly(size, new String[]{"대", "중","소" }, new String[]{"L", "M", "S"});

        String numbers = StringUtils.join(balls, ",");


        Powerball powerballData = new Powerball();
        powerballData.setDate(date);
        powerballData.setRound(Integer.parseInt(round));
        powerballData.setStatus("D");
        powerballData.setTotal_round(Integer.parseInt(total_round));
        powerballData.setBalls(numbers);
        powerballData.setPowerball(powerball);
        powerballData.setSum_numbers(String.valueOf(sum_numbers));
        powerballData.setSize(size);
        powerballData.setOe(oe);
        powerballData.setPower_oe(powerball_oe);
        powerballData.setOu(ou);
        powerballData.setPower_ou(powerball_ou);
        powerballData.setSection(section);
        powerballData.setPower_section(powerball_section);

        logger.info("powerball Data : {}",powerballData);

        addParsingLog("ntry", powerballResultJson, responseCode, responseMessage, responseTime);

        boolean is_insert = powerballDAO.existCheckAndInsertPowerball(powerballData);
        logger.info("is_insert : {}, powerballData={}", is_insert, powerballData);
        if (is_insert) {
            PushMessageService.sendPowerballMessage(powerballData);
        }

    }

    private void addParsingLog(String game_type, String url, int statusCode, String statusMessage, long responseTime) {

        powerballDAO.insertCrawlingLog(
                game_type
                , url
                , statusCode
                , statusMessage
                , responseTime
        );
    }

    public static void main(String[] args) {
        try {
            Timer timer1 = new Timer();
            PowerballSecondParser powerballParser = new PowerballSecondParser();
            timer1.scheduleAtFixedRate(powerballParser, 0, 1000);

        } catch(Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }
}
