package com.pickchat.minigame.dhlottery;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pickchat.minigame.crawler.service.PushMessageService;
import com.pickchat.minigame.crawler.service.TelegramMessageService;
import com.pickchat.minigame.crawler.service.WebCrawlingService;
import com.pickchat.minigame.dhlottery.bean.Powerball;
import com.pickchat.minigame.dhlottery.config.Configuration;
import com.pickchat.minigame.dhlottery.dao.PowerballDAO;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.impl.client.BasicCookieStore;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PowerballThirdParser extends TimerTask {

    private static Logger logger = LoggerFactory.getLogger(PowerballThirdParser.class);

    private String userAgent = "Mozilla/5.0 (Linux; U; Android 4.3; en-us; SM-N900T Build/JSS15J) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";

    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    private PowerballDAO powerballDAO = new PowerballDAO();

    private SimpleDateFormat korDateFormat = null;

    ObjectMapper mapper = new ObjectMapper();

    private Map<String, String> loginCookies = new HashMap<>();
    private BasicCookieStore cookieStore = null;

    private WebCrawlingService crawlingService = null;

    private boolean isLogin = false;

    public PowerballThirdParser() {
        crawlingService = new WebCrawlingService();
        mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);

        SimpleDateFormat korDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        korDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
        String today = korDateFormat.format(new Date());

        try {
            dayCrawing(today);
        } catch(Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

    }

    public void run() {

        int second600 = (int) (System.currentTimeMillis() / 1000) % 600; //10분 단위 초

        String minutesecond = (int) Math.floor(second600 / 60) + ":" + (second600 % 60);
        LocalTime now = LocalTime.now();
        int minute1440 = now.getHour() * 60 + now.getMinute(); //하루 24시간 기준 minute 0 ~ 1440
        logger.info("minute1440=" + minute1440 + ", second600=" + second600 + ", time=" + minutesecond);
        try {

            Thread.sleep(500L); //0.5s sleep
            if (second600 >= (3 * 60) && second600 <= (3 * 60 + 1)) {
                realtimeCrawing(); //3분 00초에 한번 실행
            }

            if (second600 >= (3 * 60 + 5) && second600 <= (3 * 60 + 6)) {
                realtimeCrawing(); //3분 5초에 한번 실행
            }

            if (second600 >= (3 * 60 + 10) && second600 <= (3 * 60 + 11)) {
                realtimeCrawing(); //3분 10초에 한번 실행
            }

            if (second600 >= (3 * 60 + 10) && second600 <= (3 * 60 + 11)) {
                realtimeCrawing(); //3분 10초에 한번 실행
            }

            if (second600 >= (8 * 60) && second600 <= (8 * 60 + 1)) {
                realtimeCrawing(); //8분 15초에 한번 실행
            }

            if (second600 >= (8 * 60 + 5) && second600 <= (8 * 60 + 6)) {
                realtimeCrawing(); //8분 5초에 한번 실행
            }

            if (second600 >= (8 * 60 + 10) && second600 <= (8 * 60 + 11)) {
                realtimeCrawing(); //8분 10초에 한번 실행
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    //실시간 크롤링 처리
    public void realtimeCrawing() {
        try {
            this.parsing();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void dayCrawing(String date) throws Exception {


        String powerballResultJson = "https://livescore.co.kr/sports/score_board/new_game/get_data.php";

        Map<String, String> param = new HashMap<>();
        param.put("game", "powerball");
        param.put("mode", "daily");
        param.put("process", "day");
        param.put("start_day", date);
        param.put("end_day", "");

        Connection.Response response = null;
        Document doc = null;
        String valueString = "";

        WebCrawlingService crawlingService = new WebCrawlingService()
                .targetUrl(powerballResultJson)
                .referer("https://livescore.co.kr/sports/score_board/new_game/analysis01.php?nohead=1&t=powerball");

        valueString = crawlingService
                .postData(param)
                .execute();

        logger.info("json : {}", valueString);
        int responseCode = crawlingService.getResponseCode();
        String responseMessage = crawlingService.getResponsePhrase();
        long responseTime = crawlingService.getResponseTime();

        HashMap<String, ?> powerballDayResult =  mapper.readValue(valueString, HashMap.class);
        String few_times = String.valueOf(powerballDayResult.get("few_times"));
        ArrayList<HashMap> roundList = (ArrayList<HashMap>)powerballDayResult.get("result");

        ArrayList<Powerball> dayPowballList = new ArrayList<>();

        for (HashMap powerballResult : roundList) {
            String total_round = String.valueOf(powerballResult.get("round"));
            String round =  String.valueOf(powerballResult.get("few_times"));
            String size =  String.valueOf(powerballResult.get("num_size"));
            String oe =  String.valueOf(powerballResult.get("num_oe"));
            String ou =  String.valueOf(powerballResult.get("num_uo"));
            String section =  String.valueOf(powerballResult.get("num_area"));
            String powerball = String.valueOf(powerballResult.get("powerball"));
            String powerball_oe =  String.valueOf(powerballResult.get("powerball_oe"));
            String powerball_ou =  String.valueOf(powerballResult.get("powerball_uo"));
            String powerball_section =  String.valueOf(powerballResult.get("powerball_area"));
            String sum_numbers =  String.valueOf(powerballResult.get("sum"));
            String numbers = String.valueOf(powerballResult.get("number"));

            Powerball powerballData = new Powerball();
            powerballData.setDate(date);
            powerballData.setRound(Integer.parseInt(round));
            powerballData.setStatus("D");
            powerballData.setTotal_round(Integer.parseInt(total_round));
            powerballData.setBalls(numbers);
            powerballData.setPowerball(powerball);
            powerballData.setSum_numbers(String.valueOf(sum_numbers));
            powerballData.setSize(size);
            powerballData.setOe(oe);
            powerballData.setPower_oe(powerball_oe);
            powerballData.setOu(ou);
            powerballData.setPower_ou(powerball_ou);
            powerballData.setSection(section);
            powerballData.setPower_section(powerball_section);

            dayPowballList.add(powerballData);

        }

        dayPowballList.forEach(powerballData -> {

            logger.info("{}", powerballData);
            powerballDAO.existCheckAndInsertPowerball(powerballData);
        });

            String telegramMessage = String.format("[%s] %s일 파워볼[라스] 전체 데이타 입력, 총=%s회차 ", Configuration.getServerInstance(), date, few_times);
            new TelegramMessageService().sendMessage(telegramMessage);
    }

    /**
     * ntry powerball 최근 데이타를 파싱 처리한다.
     * result json 구조
     * {
     *   "date": "2020-09-14",
     *   "times": "1029342",
     *   "date_round": 213,
     *   "ball": [5, 24, 8, 21, 10, "9"],
     *   "pow_ball_oe": "짝",
     *   "pow_ball_unover": "언더",
     *   "def_ball_sum": "68",
     *   "def_ball_oe": "홀",
     *   "def_ball_unover": "오버",
     *   "def_ball_size": "소",
     *   "def_ball_section": "E"
     * }
     *
     * @return
     * @throws Exception
     */
    public boolean parsing() throws Exception {

        SimpleDateFormat korDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        korDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
        String date = korDateFormat.format(new Date());

        String powerballResultJson = "https://livescore.co.kr/sports/score_board/new_game/get_data_powerball.php?process=result&limit=1";
        //String valueString  = stringGetter.getString(gameRoundScheduleUrl);

        Map<String, String> param = new HashMap<>();

        param.put("process", "result");
        param.put("limit", "1");

        Connection.Response response = null;
        Document doc = null;
        String valueString = "";

        WebCrawlingService crawlingService = new WebCrawlingService()
                .targetUrl(powerballResultJson)
                .referer("https://livescore.co.kr/sports/score_board/new_game/game_powerball.php");

        valueString = crawlingService
                .postData(param)
                .execute();

        logger.info("json : {}", valueString);
        int responseCode = crawlingService.getResponseCode();
        String responseMessage = crawlingService.getResponsePhrase();
        long responseTime = crawlingService.getResponseTime();

        HashMap<String, ?> powerballRecentResult =  mapper.readValue(valueString, HashMap.class);
        HashMap<String, ?> powerballResult =  (HashMap<String, ?>)powerballRecentResult.get("result");

        powerballResult.forEach((k, v) -> {
            HashMap<String, ?> powerballRoundResultMap = (HashMap<String, ?>) v;
            String total_round = String.valueOf(powerballRoundResultMap.get("round"));
            String round =  String.valueOf(powerballRoundResultMap.get("few_times"));
            String size =  String.valueOf(powerballRoundResultMap.get("num_size"));
            String oe =  String.valueOf(powerballRoundResultMap.get("num_oe"));
            String ou =  String.valueOf(powerballRoundResultMap.get("num_uo"));
            String section =  String.valueOf(powerballRoundResultMap.get("num_area"));
            String powerball = String.valueOf(powerballRoundResultMap.get("powerball"));
            String powerball_oe =  String.valueOf(powerballRoundResultMap.get("powerball_oe"));
            String powerball_ou =  String.valueOf(powerballRoundResultMap.get("powerball_uo"));
            String powerball_section =  String.valueOf(powerballRoundResultMap.get("powerball_area"));
            String sum_numbers =  String.valueOf(powerballRoundResultMap.get("sum"));
            String numbers = String.valueOf(powerballRoundResultMap.get("number"));

            Powerball powerballData = new Powerball();
            powerballData.setDate(date);
            powerballData.setRound(Integer.parseInt(round));
            powerballData.setStatus("D");
            powerballData.setTotal_round(Integer.parseInt(total_round));
            powerballData.setBalls(numbers);
            powerballData.setPowerball(powerball);
            powerballData.setSum_numbers(String.valueOf(sum_numbers));
            powerballData.setSize(size);
            powerballData.setOe(oe);
            powerballData.setPower_oe(powerball_oe);
            powerballData.setOu(ou);
            powerballData.setPower_ou(powerball_ou);
            powerballData.setSection(section);
            powerballData.setPower_section(powerball_section);

            logger.info("powerball Data : {}",powerballData);

            addParsingLog("livescore", powerballResultJson, responseCode, responseMessage, responseTime);

            boolean is_insert = powerballDAO.existCheckAndInsertPowerball(powerballData);
            logger.info("is_insert : {}, powerballData={}", is_insert, powerballData);
            if (is_insert) {
                PushMessageService.sendPowerballMessage(powerballData);
            }
        });
        return false;
    }

    private void addParsingLog(String game_type, String url, int statusCode, String statusMessage, long responseTime) {

        powerballDAO.insertCrawlingLog(
                game_type
                , url
                , statusCode
                , statusMessage
                , responseTime
        );
    }

    public static void main(String[] args) {
        try {
            Timer timer1 = new Timer();
            PowerballThirdParser powerballParser = new PowerballThirdParser();
            timer1.scheduleAtFixedRate(powerballParser, 10 * 1000, 1000); //실행

        } catch(Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }
}
