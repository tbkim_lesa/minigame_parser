package com.pickchat.minigame.dhlottery;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pickchat.minigame.dhlottery.bean.*;
import com.pickchat.minigame.dhlottery.dao.PowerballDAO;
import com.pickchat.minigame.dhlottery.dao.SpeedkenoDAO;
import com.pickchat.minigame.queue.MessageQueueProducer;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by taebong.kim on 2019.06.09
 */
public class SpeedKenoParser extends TimerTask {

    private int lastPageNum = 1;
    private int startPageNum = 1;


    private Logger logger = LoggerFactory.getLogger(SpeedKenoParser.class);

    private String userAgent = "Mozilla/5.0 (Linux; U; Android 8.1; en-us; SM-N900T Build/JSS15J) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.40";

    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    private SpeedkenoDAO speedKenoDAO = new SpeedkenoDAO();

    private SimpleDateFormat korDateFormat = null;

    MessageQueueProducer messageQueueProducer = new MessageQueueProducer();
    ObjectMapper mapper = new ObjectMapper();


    public SpeedKenoParser(String date) {
        mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);

        korDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        korDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

        if(date != null) {
            saveDayData(date);
        }
    }

    public void run() {

        String date = korDateFormat.format(new Date());

        int second600 = (int) (System.currentTimeMillis() / 1000) % 600; //10분 단위 초

        String minutesecond = (int) Math.floor(second600 / 60) + ":" + (second600 % 60);
        logger.info("second600=" + second600 + ", time=" + minutesecond);

        //과도한 트래픽을 발생시켜 차단당할수 있음으로, 불피요한 트래픽을 만들어서는 안됨
        //서버의 사간값을 항상 잘 맞춰두어야 함.
        //앞뒤로 1-2초정도 여유를 두어 호출해야함.
        //스피드키노는 00시00분부터 시작 하여 23시54분까지 288회차 진행 (1회차는 이전날짜 59분 52초정도에 출력됨)
        //결과 시간은 2분50초 ~ 3분00초, 7분50초 8분00초 (평균 53초에 결과 나옴)
        //1. 4분51초 ~ 4분55초 까지 게임결과 파싱처리,
        //2. 9분52초 ~ 9분55초 까지 게임결과 파싱처리,
        //todo 구간안에 결과값을 가져오지 못한다면, telegram 알림처리 필요

        try {
            if (second600 >= (4 * 60 + 51) && second600 <= (4 *  60 + 55)) {
                //파싱처리한다.
                List<Speedkeno> SpeedkenoList = parsing(date, 1, true);
                if (SpeedkenoList.size() > 0) {

                    Speedkeno speedkenoData = SpeedkenoList.get(0);
                    int round = speedKenoDAO.getTodayMaxRound(speedkenoData.getDate()) + 1;
                    speedkenoData.setRound(round);

                    boolean is_insert = speedKenoDAO.existCheckAndInsertSpeedkeno(speedkenoData);
                    logger.info("is_insert : "  + is_insert);
                    if (is_insert) {
                        sendPushMessage(speedkenoData);
                    }
                }
            }

            //7분55초 ~ 8분00초 까지 게임결과 파싱처리
            if (second600 >= (9 * 60 + 51) && second600 <= (9 * 60 + 55)) {
                //파싱처리한다.
                List<Speedkeno> speedkenoList = parsing(date, 1, true);
                if (speedkenoList.size() > 0) {

                    Speedkeno speedkenoData = speedkenoList.get(0);
                    int round = speedKenoDAO.getTodayMaxRound(speedkenoData.getDate()) + 1;
                    speedkenoData.setRound(round);

                    boolean is_insert = speedKenoDAO.existCheckAndInsertSpeedkeno(speedkenoData);
                    if (is_insert) {
                        sendPushMessage(speedkenoData);
                    }
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            //todo 텔레그램으로 알림 발송 처리. 최초 오류에만 발송.. 여러번 발송되지 않도록 함.
        }
    }

    public List<Speedkeno> parsing(String date, int page, boolean onlyTopRow) throws Exception {

        String time = System.currentTimeMillis() + "";

        String gameRoundResultUrl = "https://dhlottery.co.kr/gameInfo.do?method=kenoWinNoList";
        //String valueString  = stringGetter.getString(gameRoundScheduleUrl);

        Map<String, String> param = new HashMap<>();
        param.put("nowPage", page + "");
        param.put("searchDate", StringUtils.replace(date, "-", ""));
        param.put("calendar", date);
        param.put("sortType", "num");

        Connection.Response response = Jsoup.connect(gameRoundResultUrl)
                .header("referer", "https://dhlottery.co.kr/gameInfo.do?method=kenoWinNoList")
                .userAgent(userAgent)
                .timeout(3000)
                .data(param)
                .method(Connection.Method.POST)
                .execute();

        Document doc = response.parse();
        String valueString = doc.html();

        addParsingLog("speedkeno", gameRoundResultUrl, response);
        List<Speedkeno> speedkenoList = getSpeedkenoRowList(valueString, onlyTopRow);

        //최근회차가 list 앞에 위차함으로 역순으로 처리한다.
        Collections.reverse(speedkenoList);

        return speedkenoList;

    }

    public boolean addSpeedkenoData(Speedkeno speedkenoData) {

        int round = speedKenoDAO.getTodayMaxRound(speedkenoData.getDate()) + 1;
        speedkenoData.setRound(round);
        boolean is_update = speedKenoDAO.existCheckAndInsertSpeedkeno(speedkenoData);
        return is_update;
    }

    public List<Speedkeno> getSpeedkenoRowList(String valueString, boolean onlyTopRow) {
        Document document = Jsoup.parse(valueString);
        List<HashMap<String, String>> resultHashMapList = new ArrayList<HashMap<String, String>>();

        Element table = document.select(".tbl_data_col").get(0);

        Elements trElements = table.select("tbody > tr");

        ArrayList<Speedkeno> SpeedkenoDataRows = new ArrayList<>(trElements.size());

        for (Element ele : trElements) {
            //System.out.println(ele.html());

            HashMap<String, String> gameMap = new HashMap();

            try {

                logger.info(ele.html());

                String date = StringUtils.trimToEmpty(ele.select("td:eq(0)").text()); //추첨일(YYYY-MM-DD)
                String total_round = StringUtils.trimToEmpty(ele.select("td:eq(1)").text()); //전체 회차
                String winningNumberText = ele.select("td:eq(2)").html(); //당첨번호 텍스트..

                String sum_numbersText = StringUtils.trimToEmpty(ele.select("td:eq(3)").text()); //숫자합 (구간)
                String[] temp = StringUtils.replaceEach(sum_numbersText, new String[]{"(", ")"}, new String[]{"", ""}).split(" ");

                String sum_numbers = temp[0]; //숫자합
                String number_section = temp[1]; //숫자 구간
                String numbers = getWinningBallNumbers(winningNumberText);
                String sum_last_number = sum_numbers.substring(sum_numbers.length() -1);

                String oe = "";
                String ou = "";
                String size = "";
                String section = "";
                String powerball_section = "";

                int sum_num = Integer.parseInt(sum_numbers);
                int lastnum  = Integer.parseInt(sum_last_number);

                oe = lastnum % 2 == 0 ? "E" : "O";
                ou = lastnum * 10 > 45 ? "O" : "U";


                Speedkeno speedkenoData = new Speedkeno();
                speedkenoData.setDate(date);
                speedkenoData.setStatus("D");
                speedkenoData.setTotal_round(Integer.parseInt(total_round));
                speedkenoData.setNumbers(numbers);
                speedkenoData.setSum_numbers(sum_numbers);
                speedkenoData.setLast_number(sum_last_number);
                speedkenoData.setOe(oe);
                speedkenoData.setOu(ou);
                speedkenoData.setSection(number_section);


                SpeedkenoDataRows.add(speedkenoData);

                if (onlyTopRow) break; //첫줄만 가져오는 경우
            } catch (Exception e) {
                logger.error(e.getMessage(), e);

            }
        }

        return SpeedkenoDataRows;
    }

    /**
     * 정규식으로 당첨번호를 추출한다.
     * 추출된 당첨번호는 오름차순으로 정렬
     *
     * @param text
     * @return 5개의 2자리 숫자를 ,(콤마로) 구분하여 리턴. 숫자가 10보다 작으면 앞에 0을 추가
     */
    private String getWinningBallNumbers(String text) {
        Pattern pattern_date = Pattern.compile("([0-9]{2})");
        Matcher matcher = pattern_date.matcher(text);

        ArrayList<Integer> numbers = new ArrayList<>();
        ArrayList<String> winningNumbers = new ArrayList<>();

        while(matcher.find()) {
            //logger.info(String.format("match  : %s, numbers=%s", matcher.group(0), matcher.group(1)));
            numbers.add(Integer.parseInt(matcher.group(1)));
        }

        //Collections.sort(numbers); //소팅하지 않는다.
        numbers.forEach((number) -> {
            winningNumbers.add(String.format("%02d", number));
        });

        String result = StringUtils.join(winningNumbers, ",");

        logger.debug(text + "=> " + result);

        return result;
    }

    private void addParsingLog(String game_type, String url, Connection.Response response) {


        speedKenoDAO.insertCrawlingLog(
                game_type
                , url
                , response.statusCode()
                , response.statusMessage()
                , 0
        );
    }

    /**
     * rabbitmq에 알림 메세지를 전송한다.
     *
     * @param speedkenoData
     */
    private void sendPushMessage(Speedkeno speedkenoData) {


        TimelinePush timelinePush = new TimelinePush();

        SpeedkenoPush pushData = new SpeedkenoPush();
        pushData.setDate(speedkenoData.getDate());
        pushData.setRound(speedkenoData.getRound() + "");
        pushData.setTotal_round(speedkenoData.getTotal_round() + "");
        pushData.setNumbers(speedkenoData.getNumbers());
        pushData.setSum_numbers(speedkenoData.getSum_numbers());
        pushData.setOe(speedkenoData.getOe());
        pushData.setOu(speedkenoData.getOu());
        pushData.setSection(speedkenoData.getSection());
        long unixTimestamp = Instant.now().getEpochSecond();
        pushData.setTimestamp(unixTimestamp + "");


        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        logger.info("send powerball push message : "+ dateFormat.format(date) + ", data=" + pushData);

        try {
            String json = mapper.writeValueAsString(pushData);
            messageQueueProducer.sendTopicMessage(json, "pickchat/minigame/speedkeno");

            timelinePush.setData(pushData);

            timelinePush.setMessage_id(System.currentTimeMillis() + "");
            timelinePush.setMessage_type("public");
            timelinePush.setTimestamp(Instant.now().getEpochSecond() + "");
            timelinePush.setPriority("10");
            timelinePush.setMessage_to("@all");
            timelinePush.setMessage( String.format("%s %s 회차 스피드키노 데이타", pushData.getDate(), pushData.getRound()));
            timelinePush.setService_type("speedkeno");
            timelinePush.setService_type("speedkeno");
            timelinePush.setEvent_type("result");

            String timelinePushMessageJson = mapper.writeValueAsString(timelinePush);

            messageQueueProducer.sendTopicMessage(timelinePushMessageJson, "pickchat/inplay");

        } catch(Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public boolean webhook() {
        String webook_target_url = "";
        return true;
    }

    public boolean eventCall() {
        return true;
    }

    /**
     * 최근 날짜 데이타를 조회하여 데이타를 입력한다.
     */
    public void saveRecent365DayData() {

        int beforeDay = 365;

        for(int i = beforeDay; i >= 1; i--) {
            LocalDate targetDay = LocalDate.now().minusDays(i);
            String date = targetDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            saveDayData(date);
            try {
                Thread.sleep(2000L);
            } catch(Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * 최근 날짜 데이타를 조회하여 데이타를 입력한다.
     */
    public void saveDayData(String startDate, String endDate) {

        int todays = 400;

        LocalDate startLocalDate = LocalDate.parse(startDate);
        for(int i = 0; i < todays; i++) {
            LocalDate targetDay = startLocalDate.plusDays(i);
            String parseDate = targetDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            //saveDayData(date);
            System.out.println("parseDate = " + parseDate);
            try {
                Thread.sleep(2000L);
            } catch(Exception ex) {
                ex.printStackTrace();
            }

            if(parseDate.equals(endDate)) {
                break;
            }
        }
    }


    /**
     * 지정된 날짜의 모든 파워볼 결과값을 저장한다.
     * @param date
     * @return
     */
    public int saveDayData(String date) {
        int count = 0;

        for(int page = 29; page >= 1; page--) {
            try {
                List<Speedkeno> speedKenoList = parsing(date, page, false);
                for (Speedkeno speedkenoData : speedKenoList) {
                    addSpeedkenoData(speedkenoData);
                    count++;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                logger.error(ex.getMessage(), ex);
            }

        }

        return count;

    }

    public static void main(String[] args) {

        SimpleDateFormat korDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        korDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

        //String today = korDateFormat.format(new Date());

        //SpeedKenoParser parser = new SpeedKenoParser(null);
        SpeedKenoParser parser = new SpeedKenoParser(null);

         //parser.saveRecent365DayData();

        //parser.saveDayData("2014-01-02", "2013-12-31");
        //Timer timer1  = new Timer();

        //timer1.scheduleAtFixedRate(parser, 0, 500);

    }
}
