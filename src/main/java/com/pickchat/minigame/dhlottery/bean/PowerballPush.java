package com.pickchat.minigame.dhlottery.bean;

import lombok.Data;
import lombok.ToString;

@Data
@ToString

public class PowerballPush {


    private String date;            //게임일자 YYYY-mm-dd
    private String total_round;     //전체 회차정보
    private String round;           //당일 회차
    private String balls;           //당첨볼 5개숫자배열. 콤마로 구분
    private String powerball;       //파워볼
    private String sum_numbers;     //일반볼의 합산점수
    private String size;            //대중소 L:large(대), M:middle(중), S:small(소)
    private String oe;              //일반볼합 홀짝(O:홀, E:짝) //기준값
    private String power_oe;        //파워볼홀짝(O:홀,E:짝)
    private String ou;              //일반볼합 언오버(U:언더,O:오버)
    private String power_ou;        //파워볼 언오버(U:언더,O:오버)
    private String section;         //숫자합 구간(A ~ F)
    private String power_section;   //파워볼 구간(A ~ F)
    private String timestamp;       //현재 시간

}
