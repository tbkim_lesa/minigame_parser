package com.pickchat.minigame.dhlottery.bean;

import lombok.Data;

/**
 * Created by tbkim on 2016-06-20.
 */
@Data
public class ProtoGame {

    private String pg_id = "";
    private String year_round = "";
    private String game_idx = ""; //게임번호
    private String league_name = ""; //리그명
    private String game_type = ""; //경기타입
    private String game_date = ""; //경기일자
    private String game_time = ""; //경기시간
    private String game_datetime = ""; //경기시간
    private String game_place = "";//게임장소
    private String betting_type = "";
    private String home_name = "";
    private String away_name = "";
    private String handi_rate = "";

    private String win_odds = "";
    private String draw_odds = "";
    private String lose_odds = "";
    private String under_odds = "";
    private String over_odds = "";

    private String game_result = "";

}