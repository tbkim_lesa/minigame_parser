package com.pickchat.minigame.dhlottery.bean;

import lombok.Data;
import lombok.ToString;

@Data
@ToString

public class SpeedkenoPush {
    private String date;            //게임일자 YYYY-mm-dd
    private String total_round;     //전체 회차정보
    private String round;           //당일 회차
    private String numbers;         //당첨번호 22개
    private String sum_numbers;     //당첨번호 숫자합
    private String oe;              //당첨번호 마지막숫자 홀짝(O:홀, E:짝)
    private String ou;              //당첨번호 마지막숫자 언오버(U:언더,O:오버) 기준값 4.5
    private String section;         //숫자합 구간
    private String timestamp;       //현재 시간
}
