package com.pickchat.minigame.dhlottery.bean;

import lombok.Data;

@Data
public class ProtoRound {
    private String year;
    private String round;
    private String year_round;
    private String target_url;

}
