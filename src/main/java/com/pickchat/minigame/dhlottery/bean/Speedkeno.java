package com.pickchat.minigame.dhlottery.bean;

import lombok.Data;

import java.util.Arrays;

@Data
public class Speedkeno {

    private int no;                //순번
    private String date;            //게임일자 YYYY-mm-dd
    private Integer round;          //당일 회차
    private Integer total_round;    // 전체 회차unique
    private String status;          //상태 (R:대기, D:완료, C:취소)
    private String numbers;         //당첨볼 20개
    private String last_number;     //마지막 숫자
    private String sum_numbers;     //숫자 합산점수
    private String oe;              //마지막 숫자 홀짝(O:홀, E:짝) //기준값
    private String ou;              //마지막 숫자 언오버(U:언더,O:오버)
    private String section;         //숫자합 구간(1 ~ 9)
}
