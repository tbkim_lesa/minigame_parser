package com.pickchat.minigame.dhlottery.bean;

import lombok.Data;

/**
 * 타임라인쪽에 보내지는 스코어를 받음
 */

@Data
public class TimelinePush {
    private String message_id = "";
    private String service_type = "";
    private String event_type = "";
    private Object data;
    private String message_type = "";
    private String message_to = "";
    private String priority = "";
    private String message = "";
    private String timestamp = "";

}
