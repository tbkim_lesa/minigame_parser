package com.pickchat.minigame.dhlottery.db;


import com.pickchat.minigame.dhlottery.config.Configuration;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;

import org.apache.log4j.Logger;

/**
 * session 정보를 가져옵니다.
 */
public class SqlSessionManager {

    public static String mode = Configuration.getSpringProfilesActive();
    public static org.apache.log4j.Logger logger = Logger.getLogger(SqlSessionManager.class);

    public static HashMap<String, SqlSessionFactory> mSqlSessionMap = new HashMap();

    public static SqlSessionFactory sqlSession;

    public static SqlSessionFactory newdbSqlSession;


    static {
        String resource = "mybatis/Configuration.xml";
        Reader reader;

        String server_instance = Configuration.getServerInstance();
        try {

            reader = Resources.getResourceAsReader(resource);

            if(server_instance.equals("prd")){
                sqlSession = new SqlSessionFactoryBuilder().build(reader, "prd");
            } else if( server_instance.equals("beta")){
                sqlSession = new SqlSessionFactoryBuilder().build(reader, "beta");
            } else if( server_instance.equals("dev")){
                sqlSession = new SqlSessionFactoryBuilder().build(reader, "dev");
            }else if( server_instance.equals("local")) {
                sqlSession = new SqlSessionFactoryBuilder().build(reader, "local");
            }else{
                logger.info("mode can be 'dev' or 'beta' or 'prd'. you've inserted that : " + mode);
                sqlSession = new SqlSessionFactoryBuilder().build(reader, "local");
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }


    public static SqlSessionFactory getSqlSession() {
        return sqlSession;
    }

    public static SqlSessionFactory getNewSqlSession() {
        return newdbSqlSession;
    }

}

