package com.pickchat.minigame.dhlottery.client;

/**
 * http 응답 구조
 */
public class CustomHttpResponse {
    private int responseCode;
    private String body = "";
    private String errorMessage = "";
    private long responseMillisecond = 0L;

    public CustomHttpResponse(int responseCode, String body) {

        this(responseCode, body, 0, "");
    }


    public CustomHttpResponse(int responseCode, String body, long responseMillisecond) {
        this(responseCode, body, responseMillisecond, "");
    }

    public CustomHttpResponse(int responseCode, String body, String errorMessage) {
        this(responseCode, body, 0L, errorMessage);
    }

    public CustomHttpResponse(int responseCode, String body, long responseMillisecond,  String errorMessage) {
        this.responseCode = responseCode;
        this.body = body;
        this.responseMillisecond = responseMillisecond;
        this.errorMessage = errorMessage;
    }


    public int getResponseCode() {
        return this.responseCode;
    }

    public String getBody() {
        return this.body;
    }

    public long getResponseMillisecond() {
        return this.responseMillisecond;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    @Override
    public String toString() {
        return "HttpResponse{" +
                "responseCode=" + responseCode +
                ", body='" + body + '\'' +
                ", responseMillisecond='" + responseMillisecond + '\'' +
                ", error_message='" + errorMessage + '\'' +
                '}';
    }
}