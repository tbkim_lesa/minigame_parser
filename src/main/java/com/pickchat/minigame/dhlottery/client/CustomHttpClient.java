package com.pickchat.minigame.dhlottery.client;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.pickchat.minigame.Exception.ClientException;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class CustomHttpClient  {

    private static final Logger logger = LoggerFactory.getLogger(CustomHttpClient.class);

    private static String[] http_host = null;
    private static int http_port = 80;
    private static String protocol = "http";
    private static String api_key = "";
    private static int http_timeout_milisecond = 5000;
    private static String referer = "";


    //referer http://ntry.com/scores/powerkeno_ladder/main.php

    private static final String content_type = "application/json;charset=UTF-8";

    private String uri;
    private Map<String, Object> param = null;

    private ObjectMapper om = new ObjectMapper();


    static {

        String resource = "setting.properties";
        Properties prop = new Properties();

        String server_instance = "dev"; //인스턴스 dev : 개발환경, prd : 운영환경

        try {
            InputStream in = CustomHttpClient.class.getClassLoader().getResourceAsStream(resource);
            prop.load(in);
            in.close();

            http_host = prop.getProperty("http_host") != null ? prop.getProperty("http_host").split(",") : "127.0.0.1".split(",");
            http_port = prop.getProperty("http_port") != null ? Integer.parseInt(prop.getProperty("http_port")) : 80;
            http_timeout_milisecond = prop.getProperty("http_timeout_milisecond") != null ? Integer.parseInt(prop.getProperty("http_timeout_milisecond")) : 5000;
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-2);
        }
    }

    private  CloseableHttpClient createHttpClient() {

        Header header = new BasicHeader(
                HttpHeaders.CONTENT_TYPE, content_type);
        List<Header> headers = Lists.newArrayList(header);
        //headers.add(new BasicHeader("blockchain-api-key",api_key));

        headers.add(new BasicHeader("Referer", "http://ntry.com/scores/powerkeno_ladder/main.php"));

        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(http_timeout_milisecond)
                .setSocketTimeout(http_timeout_milisecond)
                .build();

        CloseableHttpClient httpClient = HttpClientBuilder
                .create()
                .setDefaultRequestConfig(requestConfig)
                .setDefaultHeaders(headers)
                .build();

        return httpClient;

    }

    public CustomHttpClient setParam(Map<String, Object> param ) {
        this.param = param;
        return this;
    }

    /**
     * 서버가 여러대인 경우를 순차적으로 execute 를 호출한다.
     * 모든 서버가 오류가 발생하면,  exception 을 throw 한다.
     * @param request
     * @return
     * @throws ClientException
     * @throws URISyntaxException
     */
    private HttpResponse executeMethod(String uri, HttpRequestBase request) throws ClientException, URISyntaxException {
        HttpClient httpClient = this.createHttpClient();
        for(int count = 0; count < 2; count++) { //retry
            for (int i = 0; i < http_host.length; i++) {
                try {

                    String host = StringUtils.trimToEmpty(this.http_host[i]);
                    String url = protocol + "://" + host +  uri;
                    request.setURI(new URI(url));
                    logger.info(String.format("api execute request : %s", request));

                    HttpResponse httpResponse = httpClient.execute(request);
                    return httpResponse;
                } catch (IOException ex) {
                    logger.error(String.format("http server connection error : %s", http_host[i]), ex);
                    continue;
                } catch (URISyntaxException ex) {
                    throw ex;
                }
            }
            try {
                Thread.sleep(100);
            } catch(InterruptedException ex) {

            }

        }
        throw new ClientException("api server not working..", 400);
    }

    public CustomHttpResponse post(String uri, Map<String, Object> param )  throws ClientException, Exception {

        try {

            HttpPost httpPost = new HttpPost();

            if(param == null) {
                throw new ClientException(String.format("post parameter invalid [uri=%s]. require parameter", uri), 500);
            }

            String jsonStr = om.writeValueAsString(param);
            StringEntity bodyEntity = new StringEntity(jsonStr, "UTF-8");
            httpPost.setEntity(bodyEntity);
            logger.info(String.format("http call method=%s, uri=%s, body=%s", "post", uri, jsonStr));

            //HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpResponse httpResponse = this.executeMethod(uri, httpPost);

            int responseCode = httpResponse.getStatusLine().getStatusCode();

            HttpEntity responseEntity = httpResponse.getEntity();
            String responseString = "";
            if (responseEntity != null) {
                responseString = EntityUtils.toString(responseEntity, "UTF-8");
            }

            logger.info(String.format("http call result. responseCode : %d, responseString : %s", responseCode, responseString));

            if (responseCode == HttpStatus.SC_OK) {
                return new CustomHttpResponse(responseCode, responseString);
            } else {
                throw new ClientException(responseString, responseCode);
            }

        } catch(ClientException ex) {
            logger.error( ex.getMessage(), ex);
            throw ex;
        } catch(Exception ex) {
            logger.error( ex.getMessage(), ex);
            throw ex;
        }

    }

    public CustomHttpResponse put(String uri, Map<String, Object> param )  throws ClientException, Exception {

        try {

            HttpPut httpPut = new HttpPut();


            if(param == null) {
                throw new ClientException(String.format("post parameter invalid [uri=%s]. require parameter", uri), 500);
            }

            String jsonStr = om.writeValueAsString(param);
            StringEntity bodyEntity = new StringEntity(jsonStr, "UTF-8");
            httpPut.setEntity(bodyEntity);
            logger.info(String.format("http call method=%s, uri=%s, body=%s", "post", uri, jsonStr));

            //HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpResponse httpResponse = this.executeMethod(uri, httpPut);

            int responseCode = httpResponse.getStatusLine().getStatusCode();

            HttpEntity responseEntity = httpResponse.getEntity();
            String responseString = "";
            if (responseEntity != null) {
                responseString = EntityUtils.toString(responseEntity, "UTF-8");
            }

            logger.info(String.format("http call result. responseCode : %d, responseString : %s", responseCode, responseString));

            if (responseCode == HttpStatus.SC_OK) {
                return new CustomHttpResponse(responseCode, responseString);
            } else {
                throw new ClientException(responseString, responseCode);
            }

        } catch(ClientException ex) {
            logger.error( ex.getMessage(), ex);
            throw ex;
        } catch(Exception ex) {
            logger.error( ex.getMessage(), ex);
            throw ex;
        }
    }

    public CustomHttpResponse get(String uri, Map<String, Object> param)  throws Exception {
        StopWatch watch = new StopWatch();
        watch.start();

        try {
            HttpGet httpGet = new HttpGet();

            String jsonStr = om.writeValueAsString(param);
            StringEntity bodyEntity = new StringEntity(jsonStr, "UTF-8");

            logger.info(String.format("http call method=%s, uri=%s", "get", uri));

            HttpResponse httpResponse = this.executeMethod(uri, httpGet);

            int responseCode = httpResponse.getStatusLine().getStatusCode();

            watch.stop();
            HttpEntity responseEntity = httpResponse.getEntity();
            String responseString = "";
            if (responseEntity != null) {
                responseString = EntityUtils.toString(responseEntity, "UTF-8");
            }

            logger.info(String.format("http call result. responseCode : %d, responseString : %s", responseCode, responseString));

            System.out.println("");

            if (responseCode == HttpStatus.SC_OK) {
                return new CustomHttpResponse(responseCode, responseString, watch.getTime() / 1000000L);
            } else {
                return new CustomHttpResponse(responseCode, responseString, watch.getTime() / 1000000L);
            }

        } catch(Exception ex) {
            watch.stop();
            logger.error( ex.getMessage(), ex);
            return new CustomHttpResponse(500, ex.getMessage(), watch.getTime() / 1000000L);
        }
    }

}