package com.pickchat.minigame.crawler.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 *
 */
public class WebhookClient {

    private static final Logger logger = LoggerFactory.getLogger(WebhookClient.class);

    private String uri;
    private Map<String, Object> param = null;

    private ObjectMapper om = new ObjectMapper();



    public WebhookClient() {

    }

    public WebhookClient(String refererUserid) {

    }


    private CloseableHttpClient createHttpClient() {


        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(1000)
                .setSocketTimeout(1000)
                .build();

        CloseableHttpClient httpClient = HttpClientBuilder
                .create()
                .setDefaultRequestConfig(requestConfig)
                .build();

        return httpClient;

    }


    public WebhookClient setParam(Map<String, Object> param ) {
        this.param = param;
        return this;
    }

    /**
     * 서버가 여러대인 경우를 순차적으로 execute 를 호출한다.
     * 모든 서버가 오류가 발생하면, api exception 을 throw 한다.
     * @param request
     * @return
     * @throws URISyntaxException
     */
    private HttpResponse executeMethod(String url, HttpRequestBase request)  throws URISyntaxException, IOException {
        HttpClient httpClient = this.createHttpClient();
        try {

            request.setURI(new URI(url));
            logger.info(String.format("webhook execute request : %s", request));

            HttpResponse httpResponse = httpClient.execute(request);
            return httpResponse;
        } catch (IOException ex) {
            logger.error(String.format("webhook server connection error : %s", url), ex);
            throw ex;
        } catch (URISyntaxException ex) {
            throw ex;
        }

    }

    public APIResponse post(String uri, Map<String, Object> param ) throws Exception {

        try {

            HttpPost httpPost = new HttpPost();

            if(param == null) {
                throw new Exception(String.format("post parameter invalid [uri=%s]. require parameter", uri));
            }

            String jsonStr = om.writeValueAsString(param);
            StringEntity bodyEntity = new StringEntity(jsonStr, "UTF-8");
            httpPost.setEntity(bodyEntity);

            //HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpResponse httpResponse = this.executeMethod(uri, httpPost);

            int responseCode = httpResponse.getStatusLine().getStatusCode();

            HttpEntity responseEntity = httpResponse.getEntity();
            String responseString = "";
            if (responseEntity != null) {
                responseString = EntityUtils.toString(responseEntity, "UTF-8");
            }

            //logger.info(String.format("webhook call result. responseCode : %d, responseString : %s", responseCode, jsonStr));

            if (responseCode != HttpStatus.SC_OK) {
                throw new Exception(responseString);
            }
            return new APIResponse(responseCode, responseString);


        } catch(Exception ex) {
            logger.error( ex.getMessage(), ex);
            throw ex;
        }

    }

    public APIResponse get(String url)  throws Exception {

        try {

            HttpGet httpGet = new HttpGet();

            HttpResponse httpResponse = this.executeMethod(url, httpGet);

            int responseCode = httpResponse.getStatusLine().getStatusCode();

            HttpEntity responseEntity = httpResponse.getEntity();
            String responseString = "";
            if (responseEntity != null) {
                responseString = EntityUtils.toString(responseEntity, "UTF-8");
            }

            logger.info(String.format("webhook call result. url : %s,",url));

            if (responseCode != HttpStatus.SC_OK) {
                throw new Exception(responseString);
            }
            return new APIResponse(responseCode, responseString);

        } catch(Exception ex) {
            logger.error( ex.getMessage(), ex);
            throw ex;
        }
    }

    public boolean webhook(String url)  {
       try {
            APIResponse response = this.get(url);
            logger.info(String.format("webhook : %s, response.code : %s, response.msg : %s",
                    url, response.getResponseCode(), response.getMessage()));
            if (response.getResponseCode() == 200) return true;
        } catch(Exception ex) {
           logger.error(ex.getMessage() + ", webhook_url : " + url, ex);
        }
        return false;
    }

    class APIResponse {
        private int responseCode;
        private String message;

        public APIResponse(int reponseCode, String message) {
            this.responseCode = reponseCode;
            this.message = message;
        }

        public int getResponseCode() {
            return this.responseCode;
        }


        public String getMessage() {
            return this.message;
        }

        @Override
        public String toString() {
            return "APIResponse{" +
                    "responseCode=" + responseCode +
                    ", message='" + message + '\'' +
                    '}';
        }
    }

}
