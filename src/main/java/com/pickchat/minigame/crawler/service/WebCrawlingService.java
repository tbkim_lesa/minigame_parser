package com.pickchat.minigame.crawler.service;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.*;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 크롤링을 하는 서비스 클래스
 */
public class WebCrawlingService {

    // Logger
    static private Logger logger = LoggerFactory.getLogger(WebCrawlingService.class);

    // 긁어올 url
    private String targetUrl = "";
    // 요청 referer
    private String referer = "";

    private String loginUrl = "";

    private int responseCode = 0;
    private String responsePhrase = "";
    private long responseTime = 0;
    private int connectionTimeout = 10 * 1000; //컨넥션 타임아웃 ms
    private int socketTimeout = 15 * 1000;

    private BasicCookieStore cookieStore = new BasicCookieStore();

    private Map<String, String> loginData = new HashMap<>();
    private Map<String, String> postData = new HashMap<>();


    public WebCrawlingService() {}

    public WebCrawlingService(String url) {
        this.targetUrl = url;
    }

    public WebCrawlingService(String url, String referer) {
        this.targetUrl = url;
        this.referer = referer;
    }

    public WebCrawlingService connectionTimeout(int timeout) {
        this.connectionTimeout = timeout;
        return this;
    }

    public WebCrawlingService loginParams(Map<String, String> params) {
        this.loginData = params;
        return this;
    }


    public WebCrawlingService loginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
        return this;
    }

    public WebCrawlingService targetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
        return this;
    }

    public WebCrawlingService referer(String referer) {
        this.referer = referer;
        return this;
    }

    public WebCrawlingService cookieStore(BasicCookieStore store) {
        this.cookieStore = store;
        return this;
    }

    public WebCrawlingService postData(Map<String, String> param) {
        this.postData = param;
        return this;
    }

    /**
     * 데이터를 긁어오는 메소드
     *
     * @return
     * @throws Exception
     */
    public String post()  {
        CloseableHttpClient httpClient = null;
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(this.connectionTimeout)
                .setCookieSpec(CookieSpecs.DEFAULT)
                .setSocketTimeout(this.socketTimeout).build();

        httpClient = HttpClients.custom()
                .setDefaultRequestConfig(requestConfig)
                .setDefaultCookieStore(this.cookieStore)
                .build();

        String responseString = null;
        int responseCode = 0;

        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            HttpPost httpPost = new HttpPost(this.targetUrl);
            httpPost.addHeader("Origin", "https://www.dhlottery.co.kr");
            httpPost.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
            httpPost.addHeader("Accept-Encoding", "gzip, deflate, br");
            httpPost.addHeader("Accept-Language", "ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7,ja;q=0.6");
            httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
            httpPost.addHeader("Referer", this.referer);
            httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36");

            if(this.postData != null && this.postData.size() > 0) {
                List<NameValuePair> params = new ArrayList<>();
                this.postData.forEach((k, v) -> params.add(new BasicNameValuePair(k,v)));
                httpPost.setEntity(new UrlEncodedFormEntity(params));
            }

            HttpResponse httpResponse = httpClient.execute(httpPost);
            this.responseCode = httpResponse.getStatusLine().getStatusCode();
            this.responsePhrase = httpResponse.getStatusLine().getReasonPhrase();
            stopWatch.stop();
            this.responseTime = stopWatch.getTime();

            if (this.responseCode != 200) {
                logger.error("response error. httpStatus={}, url={}", responseCode, this.targetUrl);
            }

            HttpEntity responseEntity = httpResponse.getEntity();

            if (responseEntity != null) {
                responseString = EntityUtils.toString(responseEntity, "UTF-8");
            }

        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        } finally {
            try {
                httpClient.close();
            } catch (IOException ioException) {
                logger.error("http client close io exception : {}", ioException);
            } catch (Exception exception) {
                logger.error("http client close exception : {}", exception);
            }
        }
        return responseString;
    }

    public BasicCookieStore login() {
        Map<String, String> cookies = new HashMap<>();


        CloseableHttpClient httpClient = null;
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(this.connectionTimeout)
                .setCookieSpec(CookieSpecs.DEFAULT)
                .setSocketTimeout(this.socketTimeout).build();

        httpClient = HttpClients.custom()
                .setDefaultRequestConfig(requestConfig)
                .setDefaultCookieStore(this.cookieStore)
                .setRedirectStrategy(new LaxRedirectStrategy())
                .build();

        String responseString = null;

        List<NameValuePair> params = new ArrayList<>();

        this.loginData.forEach((k, v) -> params.add(new BasicNameValuePair(k,v)));

        HttpPost httpPost = new HttpPost(this.loginUrl);
        try {
            httpPost.addHeader("Origin", "https://www.dhlottery.co.kr");
            httpPost.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
            httpPost.addHeader("Accept-Encoding", "gzip, deflate, br");
            httpPost.addHeader("Accept-Language", "ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7,ja;q=0.6");
            httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
            httpPost.addHeader("Referer", this.referer);
            httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36");


            httpPost.setEntity(new UrlEncodedFormEntity(params));
            //httpPost.setEntity(new StringEntity("password=k9400275!@&checkSave=on&userId=immerong&rememberLoginId=&newsEventYn="));

            HttpResponse httpResponse = httpClient.execute(httpPost);

            HttpEntity responseEntity = httpResponse.getEntity();

            if (responseEntity != null) {
                responseString = EntityUtils.toString(responseEntity, "UTF-8");
                //System.out.println(responseString);
            }



        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        } finally {
            try {
                httpClient.close();
            } catch (IOException ioException) {
                logger.error("http client close io exception : {}", ioException);
            } catch (Exception exception) {
                logger.error("http client close exception : {}", exception);
            }
        }
        return this.cookieStore;
    }

    public String execute() {
        return this.post();
    }

    public int getResponseCode() {
        return this.responseCode;
    }

    public String getResponsePhrase() {
        return this.responsePhrase;
    }

    public long getResponseTime() {
        return this.responseTime;
    }

    public static void main(String[] args) {
        Map<String, String> data = new HashMap<>();

        data.put("userId", "immerong");
        data.put("password", "k9400275!@");
        data.put("rememberLoginId", "");
        data.put("newsEventYn", "");
        data.put("checkSave", "on");

        BasicCookieStore cookieStore = new WebCrawlingService()
                .loginParams(data)
                .loginUrl("https://www.dhlottery.co.kr/userSsl.do?method=login")
                .referer("https://www.dhlottery.co.kr")
                .login();

        List list = cookieStore.getCookies();

        System.out.println("list of cookies");
        list.forEach((k) -> {
            System.out.println("cookie=" + k);
        });

        WebCrawlingService crawlingService = new WebCrawlingService()
                .targetUrl("https://dhlottery.co.kr/gameInfo.do?method=powerWinNoList")
                .referer("https://www.dhlottery.co.kr")
                .cookieStore(cookieStore);

        String date = "2020-07-10";
        int page = 5;
        Map<String, String> param = new HashMap<>();
        param.put("nowPage", page + "");
        param.put("searchDate", StringUtils.replace(date, "-", ""));
        param.put("calendar", date);
        param.put("sortType", "num");

        String body = crawlingService
                .postData(param)
                .cookieStore(cookieStore)
                .execute();

        System.out.println(body);
    }

    //https://www.dhlottery.co.kr/user.do?method=login
    //https://www.dhlottery.co.kr/userSsl.do?method=login
//1010341

}