package com.pickchat.minigame.crawler.service;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pickchat.minigame.dhlottery.PowerballSecondParser;
import com.pickchat.minigame.dhlottery.bean.Powerball;
import com.pickchat.minigame.dhlottery.bean.PowerballPush;
import com.pickchat.minigame.dhlottery.bean.TimelinePush;
import com.pickchat.minigame.queue.MessageQueueProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

public class PushMessageService {

    private static Logger logger = LoggerFactory.getLogger(PushMessageService.class);


    private static final MessageQueueProducer messageQueueProducer = new MessageQueueProducer();
    private static final ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
    }

    public static void
    sendPowerballMessage(Powerball powerballData) {

        TimelinePush timelinePush = new TimelinePush();

        PowerballPush pushData = new PowerballPush();
        pushData.setDate(powerballData.getDate());
        pushData.setRound(powerballData.getRound() + "");
        pushData.setTotal_round(powerballData.getTotal_round() + "");
        pushData.setBalls(powerballData.getBalls());
        pushData.setPowerball(powerballData.getPowerball());
        pushData.setSum_numbers(powerballData.getSum_numbers());
        pushData.setSize(powerballData.getSize());
        pushData.setOe(powerballData.getOe());
        pushData.setPower_oe(powerballData.getPower_oe());
        pushData.setOu(powerballData.getOu());
        pushData.setPower_ou(powerballData.getPower_ou());
        pushData.setSection(powerballData.getSection());
        pushData.setPower_section(powerballData.getPower_section());
        long unixTimestamp = Instant.now().getEpochSecond();
        pushData.setTimestamp(unixTimestamp + "");


        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        logger.info("send powerball push message : " + dateFormat.format(date) + ", data=" + pushData);

        try {
            String json = mapper.writeValueAsString(pushData);
            messageQueueProducer.sendTopicMessage(json, "pickchat/minigame/powerball");


            timelinePush.setData(pushData);

            timelinePush.setMessage_id(System.currentTimeMillis() + "");
            timelinePush.setMessage_type("public");
            timelinePush.setTimestamp(Instant.now().getEpochSecond() + "");
            timelinePush.setPriority("10");
            timelinePush.setMessage_to("@all");
            timelinePush.setMessage(String.format("%s %s 회차 파워볼 데이타", pushData.getDate(), pushData.getRound()));
            timelinePush.setService_type("powerball");
            timelinePush.setEvent_type("result");

            String timelinePushMessageJson = mapper.writeValueAsString(timelinePush);

            messageQueueProducer.sendTopicMessage(timelinePushMessageJson, "pickchat/inplay");

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

    }
}
