package com.pickchat.minigame.crawler.service;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class TelegramMessageService {


    private String baseUrl = "https://api.telegram.org/bot";

    //tokenKey는 텔레그램 봇의 키값입니다.
    private String tokenKey = "860946277:AAGb4sRHQXebYuNoRCjNpq0XVwslR9qsFw0"; //허비봇

    //채팅방의 chat_id 입니다.
    //봇을 초대하여 참여한 방의 chat_id 입니다. 봇 초대후 메세지를 보내면 chat_id를 조회할수 있습니다.
    //chat_id 는 방마다 생성됨으로, 방을 용도별로 여러개 생성하고(각 방별로 봇을 초대함), 각기 다른 용도의
    //메세지를 발송할수 있습니다.
    //private String defaultChatId = "-283650230";  //기본 chat_id, chat_id 를 하나만 사용하는 경우 이 값을 입력하세요. 그룹방은 보통 -로 시작합니다. 9자리 숫자로 구성됩니다.
    private String defaultChatId[]  = new String[]{"-422962701", ""};

    private String callUrl = "";

    public TelegramMessageService() {
        this.callUrl = this.baseUrl + this.tokenKey;
    }


    private boolean sendMessage(String msg, String[] chatIdList) {

        if(chatIdList == null) return false;

        int sendCount = 0;

        for(String chatId : chatIdList) {
            if(!chatId.equals("")) {
                sendCount += sendMessage(msg, chatId) ? 1 : 0;
            }
        }

        return sendCount > 0;
    }

    public boolean sendMessage(String msg) {
        return this.sendMessage(msg, this.defaultChatId);
    }

    /**
     * @param msg    텔레그램으로 보낼 메세지
     * @param chatId 챗아이디, 지정하지 않으면, 기본 chat_id로 설정.
     * @return bool 발송 성공 여부
     */
    public boolean sendMessage(String msg, String chatId) {
        boolean result = false;

        if (chatId == null || chatId.equals("")) {
            return false;
        }

        if (msg == null || msg.equals("")) {
            return false;
        }

        try {
            String urlString = String.format("%s/sendMessage?chat_id=%s&text=%s",
                    this.callUrl, chatId, URLEncoder.encode(msg, "UTF-8"));

            URL url = new URL(urlString);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");

            int statusCode = conn.getResponseCode();
            System.out.println("Response from Telegram Gateway: \n");
            System.out.println("Status Code: " + statusCode);
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (statusCode == 200) ? conn.getInputStream() : conn.getErrorStream()
            ));
            String output;
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }
            conn.disconnect();

            return statusCode == 200;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    public static void main(String[] args) {
        new TelegramMessageService().sendMessage("안녕하세요.^^ 메세지 전송 테스트입니다.");
    }
}