import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.DefaultHttpHeaders;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.websocketx.*;
import io.netty.handler.codec.http.websocketx.extensions.WebSocketClientExtensionHandler;
import io.netty.handler.codec.http.websocketx.extensions.compression.DeflateFrameClientExtensionHandshaker;
import io.netty.handler.codec.http.websocketx.extensions.compression.PerMessageDeflateClientExtensionHandshaker;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import io.netty.util.CharsetUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.net.URI;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Timer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * bet365에서 경기의 오버뷰 데이타를수신하는 웹소켓 클라이언트
 * 1. 수신된 overview 데이타를 bet365_live에 insert / update 처리 (미구현)
 * 2. 경기 상태 변경, 스코어 변경, 시간 변경 정보를 event push (미구현)
 * 3. SportsName에 해당하는 InplayClient 에 경기 정보를 전달한다. (미구현)
 *
 * @author k9400275@naver.com
 */
public class PowerballGameClientTest {

    private static final Logger logger = LoggerFactory.getLogger(PowerballGameClientTest.class);

    // log String ansi color code
    /*
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
    public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
    public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
    public static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
    public static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
    public static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
    public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";
*/


    private Channel ch;

    private static final EventLoopGroup group = new NioEventLoopGroup();

    private String cookieSessionid = "";
    private String generatedToken = "";
    private String secondToken = "";


    private boolean isFirst = false;
    private boolean bGetOverview = false;

    private ChannelHandlerContext bet365ctx = null;
    private ChannelHandlerContext bet365subctx = null;

    private Timer reconnectTimer = null;

    private Timer matchRequestTimer = new Timer();

    private static int mappingTimercount = 0;

    private HashSet<String> gamesSet = new HashSet<>();
    private HashMap<String, String> eventID_IT = new HashMap<>(); //eventID와, eventIT를 쌍으로 하는 맵 데이타


    public PowerballGameClientTest() throws Exception {
        init();
    }

    public void init() {

    }

    public void open() throws Exception {

        int port = 443;
        String host = "nchat.powerballgame.co.kr";
        String roomIdx = "1f174c4b117b295af7db8e581631f866";
        String token = "bd3a59a029d445d0c0ee46686c5ee2de";
        String userToken ="210dffdbeb4ffb768019f2dc508748be";

        String str = String.format("wss://nchat.powerballgame.co.kr/socket.io/?roomIdx=%s&token=%s&EIO=3&transport=websocket", roomIdx, token);
        URI uri = new URI(str);



        isFirst = true;

        SslContext sslCtx = SslContextBuilder.forClient().trustManager(InsecureTrustManagerFactory.INSTANCE).build();
        // Connect with V13 (RFC 6455 aka HyBi-17). You can change it to V08 or V00.
        // If you change it to V00, ping is not supported and remember to change
        // HttpResponseDecoder to WebSocketHttpResponseDecoder in the pipeline.

        DefaultHttpHeaders customHeaders = new DefaultHttpHeaders();
        customHeaders.add("Accept-Encoding", "gzip, deflate, br");
        customHeaders.add("Accept-Language", "en-US,en;q=0.9");
        customHeaders.add("Cache-Control", "no-cache");
        customHeaders.add("Sec-WebSocket-Extensions", "permessage-deflate; client_max_window_bits");
        customHeaders.add("Connection", "Upgrade");
        customHeaders.add("Pragma", "no-cache");
        customHeaders.add("Sec-WebSocket-Version", "13");
        customHeaders.add("Upgrade", "websocket");
        customHeaders.add("Origin", "http://www.powerballgame.co.kr");
        customHeaders.add("Host", "nchat.powerballgame.co.kr");

        customHeaders.add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36");


        final WebSocketClientHandshaker handshaker = WebSocketClientHandshakerFactory.newHandshaker(
                uri, WebSocketVersion.V13, null, true, new DefaultHttpHeaders());

        Bootstrap b = new Bootstrap();
        b.group(group)
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) {
                        ChannelPipeline p = ch.pipeline();
                        if (sslCtx != null) {
                            p.addLast(sslCtx.newHandler(ch.alloc(), host, port));
                        }
                        p.addLast(new HttpClientCodec());
                        p.addLast(new HttpObjectAggregator(65535));
                        p.addLast(new WebSocketClientExtensionHandler(
                                new PerMessageDeflateClientExtensionHandshaker(6, true, 15, false, false),
                                new DeflateFrameClientExtensionHandshaker(true)
                        ));
                        p.addLast(
                                new SimpleChannelInboundHandler<Object>() {
                                    ChannelPromise handshakeFuture;

                                    @Override
                                    public void handlerAdded(final ChannelHandlerContext ctx) throws Exception {
                                        handshakeFuture = ctx.newPromise();
                                    }

                                    @Override
                                    public void channelActive(final ChannelHandlerContext ctx) throws Exception {
                                        handshaker.handshake(ctx.channel());
                                        bet365ctx = ctx;
                                    }

                                    @Override
                                    public void channelInactive(final ChannelHandlerContext ctx) throws Exception {
                                        logger.warn("WebSocket Client disconnected!");
                                        ctx.close();
                                    }

                                    @Override
                                    public void exceptionCaught(final ChannelHandlerContext ctx, final Throwable cause) throws Exception {
                                        cause.printStackTrace();
                                        logger.error(cause.getMessage(), cause);
                                        if (!handshakeFuture.isDone()) {
                                            handshakeFuture.setFailure(cause);
                                        }
                                        ctx.close();
                                    }

                                    @Override
                                    protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
                                        final Channel ch = ctx.channel();

                                        if (!handshaker.isHandshakeComplete()) {
                                            // web socket client connected

                                            handshaker.finishHandshake(ch, (FullHttpResponse) msg);
                                            handshakeFuture.setSuccess();

                                            //WebSocketFrame frame = new TextWebSocketFrame(strParam);
                                            //ctx.channel().writeAndFlush(frame);
                                            return;
                                        }

                                        logger.info("channelRead0 : {}", msg);

                                        final WebSocketFrame frame = (WebSocketFrame) msg;
                                        if (frame instanceof TextWebSocketFrame) {
                                            final TextWebSocketFrame textFrame = (TextWebSocketFrame) frame;
                                            // uncomment to print request
                                            logger.info(textFrame.text());

                                            String textMessage = textFrame.text();

                                            if(isFirst == true) {
                                                String text = String.format("40/room?roomIdx=%s&token=%s,", roomIdx, token);
                                                isFirst = false;
                                                sendToServer(text);
                                            } if(textMessage.equals("40/room,")) {
                                                String text = String.format("42/room,[\"send\",{\"header\":{\"version\":\"1.0\",\"type\":\"LOGIN\"},\"body\":{\"cmd\":\"LOGIN\",\"userToken\":\"%s\",\"roomIdx\":\"%s\"}}]", userToken, roomIdx);
                                                isFirst = false;
                                                sendToServer(text);
                                            }

                                        } else if (frame instanceof PongWebSocketFrame) {
                                            logger.info("pong");
                                        } else if (frame instanceof PingWebSocketFrame) {
                                            logger.info("ping");
                                            bet365ctx.channel().writeAndFlush(new PongWebSocketFrame(frame.content().retain()));
                                        } else if (frame instanceof CloseWebSocketFrame) {
                                            logger.warn("CloseWebSocketFrame received. {}", frame);
                                            //ch.close();
                                        } else if (frame instanceof BinaryWebSocketFrame) {
                                            // uncomment to print request
                                            logger.warn("binaryMessage : " + frame.content().toString());
                                        }

                                        if (msg instanceof FullHttpResponse) {
                                            final FullHttpResponse response = (FullHttpResponse) msg;
                                            throw new Exception("Unexpected FullHttpResponse (getStatus=" + response.getStatus() + ", content=" + response.content().toString(CharsetUtil.UTF_8) + ')');
                                        }
                                    }
                                }
                        );
                    }
                });

        logger.info("WebSocket Client connecting");
        ch = b.connect(host, port).sync().channel();
    }


    public boolean sendToServer(String text) {
        logger.info("{} send to server  : {} {} ", text);
        if (bet365ctx == null || !bet365ctx.channel().isOpen() || !bet365ctx.channel().isActive()) {
            return false;
        }
        try {
            WebSocketFrame sendFrame = new TextWebSocketFrame(text);
            bet365ctx.channel().writeAndFlush(sendFrame);
            return true;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

        return false;
    }

    public void close() throws InterruptedException {
        logger.warn("WebSocket Client sending close");
        ch.writeAndFlush(new CloseWebSocketFrame());
        ch.closeFuture().sync();
        //group.shutdownGracefully();
    }




    public static void main(String[] args) {
        try {
            PowerballGameClientTest client = new PowerballGameClientTest();
            client.open();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

}