#!/bin/sh
export LANG=en_US.UTF-8
JAVA_OPTS="-Dlog4j.configuration=log4j-beta.xml"
PID=`cat program.pid`
state=`ps -ef | grep $PID | grep -v grep | wc -l`
jarfile='powerball_parser-1.0.0-SNAPSHOT-all.jar'
if [ $state -ge 1 ]
then
   echo "Minigame Parser is already running... PID : $PID"
else
   echo "Starting MinigameParser(Powerball)"
   java -server -Xms64m -Xmx256m  $JAVA_OPTS -Dserver_instance=beta -jar $jarfile $* &
   echo $! > program.pid
fi